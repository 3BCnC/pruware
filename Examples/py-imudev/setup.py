#!/usr/bin/env python

from distutils.core import setup, Extension

lines = [x for x in open("imudev_module.c").read().split("\n") if "#define" in x and "_VERSION_" in x and "\"" in x]

version = "0.1"

if len(lines) > 0:
    version = lines[0].split("\"")[1]
else:
    raise Exception("Unable to find _VERSION_ in imudev_module.c")


classifiers = ['Development Status :: 3 - Alpha',
               'Operating System :: POSIX :: Linux',
               'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
               'Intended Audience :: Developers',
               'Programming Language :: Python :: 2.6',
               'Programming Language :: Python :: 2.7',
               'Programming Language :: Python :: 3',
               'Topic :: Software Development',
               'Topic :: System :: Hardware',
               'Topic :: System :: Hardware :: Hardware Drivers']

setup(	
    name	    	= "imudev",
	version		    = version,
	description	    = "Python bindings for the Linux kernel driver imudev",
	long_description= open('README.md').read() + "\n" + open('CHANGELOG.md').read(),
	author		    = "Per Hedlund",
	author_email	= "per.hedlund@gmail.com",
	license		    = "GPLv2",
	classifiers	    = classifiers,
	url		        = "https://gitlab.com/CnC/pruware",
	ext_modules	    = [Extension("imudev", ["imudev_module.c"])]
)
