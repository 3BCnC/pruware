Python ImuDev
=============
This project contains a python module for interfacing with the BBB IMU devices from user space via the imudev linux kernel driver.

This code is based on the spidev code originally found here: (https://github.com/doceme/py-spidev)
All code is GPLv2 licensed unless explicitly stated otherwise.
    

Usage
-----

First make sure the IMU PRU firmware and the imudev kernel module have been deployed and loaded on the BBB.
See the "imudev" example.

Then run the following;

    python setup.py install
    python test.py


Settings
--------

None


Methods
-------

    open()

Connects to the device, opening `/dev/imudev`
Already included in the constructor ImuDev()

    read()

Read all bytes from the device buffer.

    write(list of values)

Writes data to the device.

    close()

Disconnects from the device.
