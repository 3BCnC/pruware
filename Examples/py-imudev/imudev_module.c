/*
 * Source Modified by Per Hedlund < per.hedlund@gmail.com >
 * Based on the spidev code distributed by Volker Thoms and Stephen Caudle
 *
 * spidev_module.c - Python bindings for Linux SPI access through spidev
 * Copyright (C) 2009 Volker Thoms <unconnected@gmx.de>
 * Copyright (C) 2012 Stephen Caudle <scaudle@doceme.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <Python.h>
#include "structmember.h"
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/types.h>
//#include <sys/ioctl.h>
//#include <linux/ioctl.h>

#define _VERSION_ "0.1"
#define TX_MAXSIZE 8
#define RX_MAXSIZE 10000


#if PY_MAJOR_VERSION < 3
#define PyLong_AS_LONG(val) PyInt_AS_LONG(val)
#define PyLong_AsLong(val) PyInt_AsLong(val)
#endif

// Macros needed for Python 3
#ifndef PyInt_Check
#define PyInt_Check			PyLong_Check
#define PyInt_FromLong		PyLong_FromLong
#define PyInt_AsLong		PyLong_AsLong
#define PyInt_Type			PyLong_Type
#endif

PyDoc_STRVAR(ImuDev_module_doc,
	"This module defines an object type that allows IMU communication\n"
	"on hosts running the Linux kernel. The host kernel must have PRU\n"
	"support and IMU device interface support.\n"
	"All of these can be either built-in to the kernel, or loaded from\n"
	"modules.\n"
	"\n"
	"Because the IMU device interface is opened R/W, users of this\n"
	"module usually must have root permissions.\n");

typedef struct {
	PyObject_HEAD

	int fd;					/* open file descriptor: /dev/imudev */
	uint8_t dummy;			/* dummy attribute for future use */
} ImuDevObject;

static PyObject *
ImuDev_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
	ImuDevObject *self;
	if ((self = (ImuDevObject *)type->tp_alloc(type, 0)) == NULL)
		return NULL;

	self->fd = -1;
	self->dummy = 0;

	Py_INCREF(self);
	return (PyObject *)self;
}

PyDoc_STRVAR(ImuDev_open_doc,
	"open()\n\n"
	"Connects the object to the specified IMU device.\n"
	"open() will open /dev/imudev\n");

static PyObject *
ImuDev_open(ImuDevObject *self)
{
	char path[20] = "/dev/imudev";

	if ((self->fd = open(path, O_RDWR, 0)) == -1) {
		PyErr_SetFromErrno(PyExc_IOError);
		return NULL;
	}

	Py_INCREF(Py_None);
	return Py_None;
}

PyDoc_STRVAR(ImuDev_close_doc,
	"close()\n\n"
	"Disconnects the object from the interface.\n");

static PyObject *
ImuDev_close(ImuDevObject *self)
{
	if ((self->fd != -1) && (close(self->fd) == -1)) {
		PyErr_SetFromErrno(PyExc_IOError);
		return NULL;
	}

	self->fd = -1;
	self->dummy = 0;
	//self->mode = 0;
	//self->bits_per_word = 0;
	//self->max_speed_hz = 0;

	Py_INCREF(Py_None);
	return Py_None;
}

static void
ImuDev_dealloc(ImuDevObject *self)
{
	PyObject *ref = ImuDev_close(self);
	Py_XDECREF(ref);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static char *wrmsg_list0 = "Empty argument list.";
static char *wrmsg_listmax = "Argument list size exceeds %d bytes.";
static char *wrmsg_val = "Non-Int/Long value in arguments: %x.";

PyDoc_STRVAR(ImuDev_write_doc,
	"write([values]) -> None\n\n"
	"Write bytes to IMU device.\n");

static PyObject *
ImuDev_write(ImuDevObject *self, PyObject *args)
{
	int			status;
	uint16_t	ii, len;
	uint8_t		buf[TX_MAXSIZE];
	PyObject	*obj;
	PyObject	*seq;
	char		wrmsg_text[TX_MAXSIZE];

	if (!PyArg_ParseTuple(args, "O:write", &obj))
		return NULL;

	seq = PySequence_Fast(obj, "expected a sequence");
	len = PySequence_Fast_GET_SIZE(obj);
	if (!seq || len <= 0) {
		PyErr_SetString(PyExc_TypeError, wrmsg_list0);
		return NULL;
	}

	if (len > TX_MAXSIZE) {
		snprintf(wrmsg_text, sizeof(wrmsg_text) - 1, wrmsg_listmax, TX_MAXSIZE);
		PyErr_SetString(PyExc_OverflowError, wrmsg_text);
		return NULL;
	}

	for (ii = 0; ii < len; ii++) {
		PyObject *val = PySequence_Fast_GET_ITEM(seq, ii);
#if PY_MAJOR_VERSION < 3
		if (PyInt_Check(val)) {
			buf[ii] = (__u8)PyInt_AS_LONG(val);
		} else
#endif
		{
			if (PyLong_Check(val)) {
				buf[ii] = (__u8)PyLong_AS_LONG(val);
			} else {
				snprintf(wrmsg_text, sizeof (wrmsg_text) - 1, wrmsg_val, val);
				PyErr_SetString(PyExc_TypeError, wrmsg_text);
				return NULL;
			}
		}
	}

	Py_DECREF(seq);
	
	status = write(self->fd, &buf[0], len);

	if (status < 0) {
		PyErr_SetFromErrno(PyExc_IOError);
		return NULL;
	}

	if (status != len) {
		perror("short write");
		return NULL;
	}

	Py_INCREF(Py_None);
	return Py_None;
}

PyDoc_STRVAR(ImuDev_read_doc,
	"read() -> [values]\n\n"
	"Read bytes from IMU device.\n");

static PyObject *
ImuDev_read(ImuDevObject *self)
{
	uint8_t		rxbuf[RX_MAXSIZE];
	int			status, len, ii;
	PyObject	*list;
	
	//if (!PyArg_ParseTuple(args, "i:read", &len))
	//	return NULL;

	/* read at least 1 byte, no more than RX_MAXSIZE */
	/*if (len < 1)
		len = 1;
	else if ((unsigned)len > sizeof(rxbuf))
		len = sizeof(rxbuf);*/

	memset(rxbuf, 0, sizeof(rxbuf));
	status = read(self->fd, &rxbuf[0], sizeof(rxbuf));

	if (status < 0) {
		PyErr_SetFromErrno(PyExc_IOError);
		return NULL;
	}
	/*
	if (status != len) {
		perror("short read");
		return NULL;
	}
	*/
	
	// first 4 bytes contains the buffer size, convert to int
    len = rxbuf[0] + (rxbuf[1] << 8) + (rxbuf[2] << 16) + (rxbuf[3] << 24);
	list = PyList_New(len);
	
	// ignore first 4 bytes which contains the buffer size
	for (ii = 0; ii < len; ii++) {
		//PyObject *val = Py_BuildValue("y#", (uint8_t)rxbuf[(ii+sizeof(len))]);
		PyObject *val = Py_BuildValue("l", (long)rxbuf[(ii+sizeof(len))]);
		//PyObject *val = Py_BuildValue("c", (char)rxbuf[ii]);
		PyList_SET_ITEM(list, ii, val);
	}

	return list;
}

PyDoc_STRVAR(ImuDev_fileno_doc,
	"fileno() -> integer \"file descriptor\"\n\n"
	"This is needed for lower-level file interfaces, such as os.read().\n");

static PyObject *
ImuDev_fileno(ImuDevObject *self)
{
	PyObject *result = Py_BuildValue("i", self->fd);
	Py_INCREF(result);
	return result;
}

/* (keeping this as an example showing how to set/get attributes via ioctl)

static int __imudev_set_mode( int fd, __u8 mode) {
	__u8 test;
	if (ioctl(fd, SPI_IOC_WR_MODE, &mode) == -1) {
		PyErr_SetFromErrno(PyExc_IOError);
		return -1;
	}
	if (ioctl(fd, SPI_IOC_RD_MODE, &test) == -1) {
		PyErr_SetFromErrno(PyExc_IOError);
		return -1;
	}
	if (test != mode) {
		return -1;
	}
	return 0;
}

static PyObject *
ImuDev_get_mode(ImuDevObject *self, void *closure)
{
	PyObject *result = Py_BuildValue("i", (self->mode & (SPI_CPHA | SPI_CPOL) ) );
	Py_INCREF(result);
	return result;
}

static int
ImuDev_set_mode(ImuDevObject *self, PyObject *val, void *closure)
{
	uint8_t mode, tmp;

	if (val == NULL) {
		PyErr_SetString(PyExc_TypeError,
			"Cannot delete attribute");
		return -1;
	}
#if PY_MAJOR_VERSION < 3
	if (PyInt_Check(val)) {
		mode = PyInt_AS_LONG(val);
	} else
#endif
	{
		if (PyLong_Check(val)) {
			mode = PyLong_AS_LONG(val);
		} else {
			PyErr_SetString(PyExc_TypeError,
				"The mode attribute must be an integer");
			return -1;
		}
	}


	if ( mode > 3 ) {
		PyErr_SetString(PyExc_TypeError,
			"The mode attribute must be an integer"
				 "between 0 and 3.");
		return -1;
	}

	// clean and set CPHA and CPOL bits
	tmp = ( self->mode & ~(SPI_CPHA | SPI_CPOL) ) | mode ;

	__spidev_set_mode(self->fd, tmp);

	self->mode = tmp;
	return 0;
}
*/

static PyGetSetDef ImuDev_getset[] = {
	/*{"mode", (getter)ImuDev_get_mode, (setter)ImuDev_set_mode,
			"SPI mode as two bit pattern of \n"
			"Clock Polarity  and Phase [CPOL|CPHA]\n"
			"min: 0b00 = 0 max: 0b11 = 3\n"},*/
	{NULL},
};

static int
ImuDev_init(ImuDevObject *self)
{
	ImuDev_open(self);
	if (PyErr_Occurred())
		return -1;

	return 0;
}

PyDoc_STRVAR(ImuDevObjectType_doc,
	"ImuDev() -> IMU\n\n"
	"Return a new IMU object that is (optionally) connected to the\n"
	"specified IMU device interface.\n");

static
PyObject *ImuDev_enter(PyObject *self, PyObject *args)
{
    //if (!PyArg_ParseTuple(args, ""))
    //    return NULL;

    Py_INCREF(self);
    return self;
}

static
PyObject *ImuDev_exit(ImuDevObject *self, PyObject *args)
{

    PyObject *exc_type = 0;
    PyObject *exc_value = 0;
    PyObject *traceback = 0;
    if (!PyArg_UnpackTuple(args, "__exit__", 3, 3, &exc_type, &exc_value,
                           &traceback)) {
        return 0;
    }

    ImuDev_close(self);
    Py_RETURN_FALSE;
}

static PyMethodDef ImuDev_methods[] = {
	{"open", (PyCFunction)ImuDev_open, METH_NOARGS,
		ImuDev_open_doc},
	{"close", (PyCFunction)ImuDev_close, METH_NOARGS,
		ImuDev_close_doc},
	{"fileno", (PyCFunction)ImuDev_fileno, METH_NOARGS,
		ImuDev_fileno_doc},
	{"read", (PyCFunction)ImuDev_read, METH_NOARGS,
		ImuDev_read_doc},
	{"write", (PyCFunction)ImuDev_write, METH_VARARGS,
		ImuDev_write_doc},
	{"__enter__", (PyCFunction)ImuDev_enter, METH_VARARGS,
		NULL},
	{"__exit__", (PyCFunction)ImuDev_exit, METH_VARARGS,
		NULL},
	{NULL},
};

static PyTypeObject ImuDevObjectType = {
#if PY_MAJOR_VERSION >= 3
	PyVarObject_HEAD_INIT(NULL, 0)
#else
	PyObject_HEAD_INIT(NULL)
	0,						/* ob_size */
#endif
	"ImuDev",				/* tp_name */
	sizeof(ImuDevObject),	/* tp_basicsize */
	0,						/* tp_itemsize */
	(destructor)ImuDev_dealloc,	/* tp_dealloc */
	0,						/* tp_print */
	0,						/* tp_getattr */
	0,						/* tp_setattr */
	0,						/* tp_compare */
	0,						/* tp_repr */
	0,						/* tp_as_number */
	0,						/* tp_as_sequence */
	0,						/* tp_as_mapping */
	0,						/* tp_hash */
	0,						/* tp_call */
	0,						/* tp_str */
	0,						/* tp_getattro */
	0,						/* tp_setattro */
	0,						/* tp_as_buffer */
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
	ImuDevObjectType_doc,		/* tp_doc */
	0,						/* tp_traverse */
	0,						/* tp_clear */
	0,						/* tp_richcompare */
	0,						/* tp_weaklistoffset */
	0,						/* tp_iter */
	0,						/* tp_iternext */
	ImuDev_methods,		/* tp_methods */
	0,						/* tp_members */
	ImuDev_getset,		/* tp_getset */
	0,						/* tp_base */
	0,						/* tp_dict */
	0,						/* tp_descr_get */
	0,						/* tp_descr_set */
	0,						/* tp_dictoffset */
	(initproc)ImuDev_init,	/* tp_init */
	0,						/* tp_alloc */
	ImuDev_new,			/* tp_new */
};

static PyMethodDef ImuDev_module_methods[] = {
	{NULL}
};

#if PY_MAJOR_VERSION >= 3
static struct PyModuleDef moduledef = {
	PyModuleDef_HEAD_INIT,
	"imudev",
	ImuDev_module_doc,
	-1,
	ImuDev_module_methods,
	NULL,
	NULL,
	NULL,
	NULL,
};
#else
#ifndef PyMODINIT_FUNC	/* declarations for DLL import/export */
#define PyMODINIT_FUNC void
#endif
#endif

#if PY_MAJOR_VERSION >= 3
PyMODINIT_FUNC
PyInit_imudev(void)
#else
void initimudev(void)
#endif
{
	PyObject* m;

	if (PyType_Ready(&ImuDevObjectType) < 0)
#if PY_MAJOR_VERSION >= 3
		return NULL;
#else
		return;
#endif

#if PY_MAJOR_VERSION >= 3
	m = PyModule_Create(&moduledef);
	PyObject *version = PyUnicode_FromString(_VERSION_);
#else
	m = Py_InitModule3("imudev", ImuDev_module_methods, ImuDev_module_doc);
	PyObject *version = PyString_FromString(_VERSION_);
#endif

	PyObject *dict = PyModule_GetDict(m);
	PyDict_SetItemString(dict, "__version__", version);
	Py_DECREF(version);

	Py_INCREF(&ImuDevObjectType);
	PyModule_AddObject(m, "ImuDev", (PyObject *)&ImuDevObjectType);

#if PY_MAJOR_VERSION >= 3
	return m;
#endif
}
