#!/usr/bin/python

import imudev
from struct import unpack, pack
import time, math

MISC_ACCEL_SCALES   = [2, 4, 8, 16] # g
MISC_GYRO_SCALES    = [250, 500, 1000, 2000] # deg/s
MISC_MAG_SCALES     = [4912] # uT
    
def __mergeData(lsb, msb):
    # Merge 2 bytes into 1 int16
    uint16 = pack('<H', ((msb&0x000000FF)<<8) + (lsb&0x000000FF))
    int16 = unpack('h', uint16)[0]
    #print "{0:b}".format(int16)
    return int16

def __scaleData(rawData, scale):
    # Scale raw int16 value to actual value
    return float(rawData) / pow(2, 16-1) * scale
        
def getAccel(data):
    scaleIdx = 0
    # Read 6 bytes for XH, XL, YH, YL, ZH, ZL
    x = __scaleData(__mergeData(data[0], data[1]), MISC_ACCEL_SCALES[scaleIdx])
    y = __scaleData(__mergeData(data[2], data[3]), MISC_ACCEL_SCALES[scaleIdx])
    z = __scaleData(__mergeData(data[4], data[5]), MISC_ACCEL_SCALES[scaleIdx])
    return [x,y,z]

def getGyro(data):
    scaleIdx = 0
    # Read 6 bytes for XH, XL, YH, YL, ZH, ZL
    x = __scaleData(__mergeData(data[0], data[1]), MISC_GYRO_SCALES[scaleIdx])
    y = __scaleData(__mergeData(data[2], data[3]), MISC_GYRO_SCALES[scaleIdx])
    z = __scaleData(__mergeData(data[4], data[5]), MISC_GYRO_SCALES[scaleIdx])
    return [x,y,z]

def getMag(data):
    # Read 6 bytes for XH, XL, YH, YL, ZH, ZL
    x = __scaleData(__mergeData(data[0], data[1]), MISC_MAG_SCALES[0])
    y = __scaleData(__mergeData(data[2], data[3]), MISC_MAG_SCALES[0])
    z = __scaleData(__mergeData(data[4], data[5]), MISC_MAG_SCALES[0])
    return [x,y,z]
    
def getTemp(data):
    # TEMP_degC = ((TEMP_OUT - RoomTemp_Offset)/Temp_Sensitivity) + 21degC
    # 2 bytes for temperature H and L
    return (__mergeData(data[0], data[1]) - 0)/333.87 + 21


sensors = 4
imu = imudev.ImuDev()

#imu.open() # open already done in constructor
imu.write([0x01]) # 1 = start sampling

while True:
    try:
        time.sleep(1)
        read_buf = imu.read()
        #print(type(read_buf[0]))

        for s in range(sensors):
            i = s*20
            print "======== Sensor %d ========" %s
            #print(read_buf)
            print("Size:    %s" %len(read_buf))
            print "Accel:   %s" %getAccel(read_buf[i+6:i+12])
            print "Gyro:    %s" %getGyro(read_buf[i+12:i+18])
            print "Mag:     %s" %getMag(read_buf[i:i+6])
            print "Temp:    %s" %getTemp(read_buf[i+18:i+20])
        print ""
        
    except KeyboardInterrupt:
        print "\nStopped by user."
        break
    
    except Exception, e:
        print "\nError: %s" %e
        break

imu.write([0x00]) # 0 = stop sampling
imu.close()