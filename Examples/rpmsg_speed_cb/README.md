
###### rpmsg_speed_cb ######
The example contains a kernel module "module/rpmsg_speed_cb.c", a firmware for pru-1 "firmware/main_pru1.c" and a user space test program "test_user_space.c". 

This module tests the speed of the rpmsg interface between the PRU and ARM in addition to storing data in a circular buffer in the module. 
The module when probed, creates a character device at "/dev/rpmsg_speed". 
Writing a number (1 or 0) to this device will start/stop the test where messages are produces on the PRU and sent to the module. 
The module stores the data in a circular buffer. Each time the user space program requests a message from the module the circular buffer is emptied.

To get the example working:

Compile the source (cd to rpmsg_speed_cb) :
        
        $ make
        $ cd firmware
        $ ./deploy.sh
        $ cd ../module
        $ sudo insmod rpmsg_speed.ko
        $ cd ..
        $ ./test

In case an error like "No rule to make target 'modules'." occurs when running the make file, there might be an issue with the paths. Try to add a symbolic link to your kernel source headers (build and src folders).

        $ uname -r
        4.4.30-ti-r64
        
        $ rm /lib/modules/4.4.30-ti-r64/build
        $ rm /lib/modules/4.4.30-ti-r64/src
        $ ln -s /usr/src/linux-headers-4.4.30-ti-r64 /lib/modules/4.4.30-ti-r64/build
        $ ln -s /usr/src/linux-headers-4.4.30-ti-r64 /lib/modules/4.4.30-ti-r64/src