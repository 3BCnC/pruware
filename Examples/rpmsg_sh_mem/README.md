
###### rpmsg_sh_mem ######

This example contains a kernel module, firmware for PRU-0 and PRU-1 and a user space test program. 

The module tests the rpmsg interface between the PRU and ARM and storing data in a circular buffer in the module. 
The module when probed, creates a character device at "/dev/rpmsg_speed". 
Writing a number (1 or 0) to this device will start/stop the test where messages are produces on the PRU and sent to the module. 
The module stores the data in a circular buffer. Each time the user space program requests a message from the module the circular buffer is emptied.
Both PRU cores are included in the test. PRU-0 produces data and stores it in the shared memory and PRU-1 reads the latest shared memory data and sends it to ARM.

To get the example working:

Compile the source (cd to rpmsg_sh_mem) :
        
        $ make
        $ cd firmware
        $ ./deploy.sh
        $ cd ../module
        $ sudo insmod rpmsg_speed.ko
        $ cd ..
        $ ./test

In case the error "No rule to make target 'modules'." occurs when running the make file, there might be an issue with the paths. 
Try to add a symbolic link to your kernel source headers (build and src folders).

        $ uname -r
        4.4.30-ti-r64
        
        $ rm /lib/modules/4.4.30-ti-r64/build
        $ rm /lib/modules/4.4.30-ti-r64/src
        $ ln -s /usr/src/linux-headers-4.4.30-ti-r64 /lib/modules/4.4.30-ti-r64/build
        $ ln -s /usr/src/linux-headers-4.4.30-ti-r64 /lib/modules/4.4.30-ti-r64/src