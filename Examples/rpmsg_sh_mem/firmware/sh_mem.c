/* 
 * Copyright (c) 2017 Per Hedlund (per.hedlund@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <string.h> // memset
#include "sh_mem.h"

// direct pointer to shared memory address
// first byte holds the mutex flag enum, rest is data
volatile uint8_t *shared_mem = (volatile uint8_t *) PRU_SHARED_MEM_ADDR;

MutexFlag_t getMutexFlag(void) {
	return (MutexFlag_t) shared_mem[0];
}

void setMutexFlag(MutexFlag_t mf) {
	shared_mem[0] = mf;
}

void writeShMem(const void *buffer, size_t size) {
	// set flag to locked
	shared_mem[0] = LOCKED;
	// write data to shared memory
	memcpy((void *) &shared_mem[1], buffer, size);
	// set flag to data available
	shared_mem[0] = DATA_AVAIL;
}

void readShMem(void *buffer, size_t size) {
	// set flag to locked
	shared_mem[0] = LOCKED;
	// read data from shared memory
	memcpy(buffer, (void *) &shared_mem[1], size);
	// reset shared memory
	memset((void *) &shared_mem[1], 0, size);
	// set flag to unlocked
	shared_mem[0] = UNLOCKED;
}

