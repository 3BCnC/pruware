/* 
 * Copyright (c) 2017 Per Hedlund (per.hedlund@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <stdint.h>
#include <pru_cfg.h>
#include "resource_table_empty.h"
#include "sh_mem.h"

// write buffer is a 2D array of uint16_t, holding 2 bytes each
#define WB_SIZE_R 4
#define WB_SIZE_C 10

void main(void)
{
	// local variables
	uint16_t writeBuffer[WB_SIZE_R][WB_SIZE_C] = {0};
	int i, j, ascii;
	
	// reset shared memory flag to unlocked
	setMutexFlag(UNLOCKED);

	/* Clear SYSCFG[STANDBY_INIT] to enable OCP master port */
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;

	/* TODO: Create stop condition, else it will toggle indefinitely */
	while (1) {

		// fill write buffer
		for (i=0; i<WB_SIZE_R; i++) {
			for (j=0; j<WB_SIZE_C; j++) {
				// only generate readable ascii between "!" and "~" (33 to 126)
				ascii = ((i*WB_SIZE_C + j) % (126-33)) + 33;
				// store 2 bytes in one uint16_t
				writeBuffer[i][j] = (writeBuffer[i][j] & 0x00FF) | ((uint16_t) (ascii+1) << 8); // MSB
				writeBuffer[i][j] = (writeBuffer[i][j] & 0xFF00) | ((uint16_t) (ascii+0) & 0x00FF); // LSB
			}
		}
		
		// check flag if not locked 
		if (getMutexFlag() != LOCKED) {
			
			// write data to shared memory
			writeShMem(&writeBuffer, sizeof(writeBuffer));
			
			// wait 1 ms between memory writes
			__delay_cycles(200000);
		}
		
	}
	
	/* Halt the PRU core - shouldn't get here */
	//__halt();
}

