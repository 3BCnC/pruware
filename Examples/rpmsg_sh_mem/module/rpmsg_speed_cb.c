/*
 * Source Modified by Per Hedlund < per.hedlund@gmail.com >
 * Based on the examples distributed by ZeekHuge
 *
 * Copyright (C) 2016 Zubeen Tolani <ZeekHuge - zeekhuge@gmail.com>
 *
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/mutex.h>
#include <linux/kernel.h>
#include <linux/rpmsg.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/module.h>
#include <linux/kfifo.h>
#include <linux/uaccess.h>
#include <linux/poll.h>
#include <linux/delay.h>

#include "circularbuffer.h"
#include "rpmsg_speed_cb.h"

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Per Hedlund <per.hedlund@gmail.com>");
MODULE_DESCRIPTION("A buffered rpmsg speed test driver for the BBB");
MODULE_VERSION("0.2");

MODULE_DEVICE_TABLE(rpmsg, rpmsg_speed_id_table);
static DEFINE_IDR(rpmsg_speed_minors);
static DEFINE_MUTEX(cb_mutex); // Mutex for circular buffer

static void send_to_pru(struct rpmsg_speed_dev *rpmsg_speed_dev) 
{
	rpmsg_speed_dev->buf_lock = true;
	ret = rpmsg_send(rpmsg_speed_dev->rpmsg_dev, write_buf, sizeof(write_buf));
	if (ret) {
		dev_err(rpmsg_speed_dev->dev, "Transmission on rpmsg bus failed %d\n", ret);
	}
	rpmsg_speed_dev->buf_lock = false;
}	

static int send_to_user(void *buf) 
{
	// reset read buffer
    memset(read_buf, 0, sizeof(read_buf));
    
    mutex_lock(&cb_mutex);
    //cb_print(&cb1);
	ret = cb_pop_all(&cb1, &read_buf);
	//ret = cb_pop_all(&cb1, rx_buf);
	mutex_unlock(&cb_mutex);
	
    if (ret) {  
    	dev_err(rpmsg_speed_dev->dev, "CB empty. Err: %d\n", ret);
    	return -EFAULT; 
	}
	
	// copy_to_user has the format (*to, *from, size) and returns 0 on success
	ret = copy_to_user(buf, &read_buf, sizeof(read_buf));
	//ret = copy_to_user(buf, rx_buf, sizeof(read_buf));
	
	if (ret) {  
    	dev_err(rpmsg_speed_dev->dev, "Failed to send data to the user. Err: %d\n", ret);
    	return -EFAULT; 
	}
	return ret;
}

static int rpmsg_speed_open(struct inode *inode, struct file *filp)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_open\n");
	ret = -EACCES;
	rpmsg_speed_dev = container_of(inode->i_cdev, struct rpmsg_speed_dev, cdev);
	/*
	if (!rx_buf) {
		rx_buf = kmalloc(READ_BUF_SIZE, GFP_KERNEL);
		if (!rx_buf) {
			dev_err(rpmsg_speed_dev->dev, "Failed to allocate rx_buf\n");
			ret = -ENOMEM;
		}
	}
	*/
	if (!rpmsg_speed_dev->dev_lock) {
		rpmsg_speed_dev->dev_lock = true;
		filp->private_data = rpmsg_speed_dev;
		ret = 0;
	}

	if (ret) {
		dev_err(rpmsg_speed_dev->dev, "Failed to open already open device\n");
	}
	
	return ret;
}

static int rpmsg_speed_release(struct inode *inode, struct file *filp)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_release\n");
	rpmsg_speed_dev = container_of(inode->i_cdev, struct rpmsg_speed_dev, cdev);
	rpmsg_speed_dev->buf_lock = false;
	rpmsg_speed_dev->dev_lock = false;
	/*
	kfree(rx_buf);
	rx_buf = NULL;
	*/
	return 0;
}

static ssize_t rpmsg_speed_read(struct file *filp, char __user *buf, size_t count, loff_t *f_ops)
{
	//printk(KERN_DEBUG "Enter rpmsg_speed_read\n");
	rpmsg_speed_dev = filp->private_data;
    
    if (!rpmsg_speed_dev->buf_lock) {
		ret = send_to_user(buf);
		
		if(ret) {
			rpmsg_speed_dev->buf_lock = false;
		}
		return ret;
    }
    dev_err(rpmsg_speed_dev->dev, "Buffer is locked\n");
	return -EBUSY;
}

static ssize_t rpmsg_speed_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_ops)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_write\n");
	rpmsg_speed_dev = filp->private_data;
	
	if (!rpmsg_speed_dev->buf_lock) {
		
		if (count > 1) {
			dev_err(rpmsg_speed_dev->dev, "Only 0 or 1 allowed, ie stop/start sampling\n");
			return -EINVAL;
		}
		
		ret = copy_from_user(write_buf, buf, count);
		if (ret) {
			dev_err(rpmsg_speed_dev->dev, "Failed to read %d data from the user\n", ret);
			return -EFAULT;
		}
		
		//dev_info(rpmsg_speed_dev->dev, "Write message : %s\n", write_buf);
		dev_info(rpmsg_speed_dev->dev, "Write message : %d\n", write_buf[0]);
		send_to_pru(rpmsg_speed_dev); // Initiate test
		
		return count;
	}

	dev_err(rpmsg_speed_dev->dev, "Buffer is locked\n");
	return -EBUSY;
}

static void rpmsg_speed_cb(struct rpmsg_channel *rpmsg_dev, void *data , int len , void *priv, u32 src)
{
	//printk(KERN_DEBUG "Enter rpmsg_speed_cb\n");
	rpmsg_speed_dev = dev_get_drvdata(&rpmsg_dev->dev);
	//dev_info(&rpmsg_dev->dev, "Incoming message : %s\n", (char*) data);
	
	mutex_lock(&cb_mutex);
	ret = cb_push_chunk(&cb1, data, len);
	mutex_unlock(&cb_mutex);
	
	if (ret) {
        dev_err(rpmsg_speed_dev->dev, "Out of space in CB. Err: %d\n", ret);
    }
}

static int rpmsg_speed_probe(struct rpmsg_channel *rpmsg_dev)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_probe\n");
	dev_info(&rpmsg_dev->dev, "Chnl: 0x%x -> 0x%x\n", rpmsg_dev->src, rpmsg_dev->dst);

	rpmsg_speed_dev = devm_kzalloc(&rpmsg_dev->dev, sizeof(*rpmsg_speed_dev), GFP_KERNEL);
	if(!rpmsg_speed_dev)
		return -ENOMEM;


	minor_obtained = idr_alloc(&rpmsg_speed_minors, rpmsg_speed_dev, 0, PRU_MAX_DEVICES, GFP_KERNEL);

	if(minor_obtained < 0) {
		ret = minor_obtained;
		dev_err(&rpmsg_dev->dev, "Failed to get a minor number with return value %d\n", ret);
		goto fail_idr_alloc;
	}

	rpmsg_speed_dev->devt = MKDEV(MAJOR(rpmsg_speed_devt), minor_obtained);

	cdev_init(&rpmsg_speed_dev->cdev, &rpmsg_speed_fops);
	rpmsg_speed_dev->cdev.owner = THIS_MODULE;
	ret = cdev_add(&rpmsg_speed_dev->cdev, rpmsg_speed_dev->devt,1);
	if (ret) {
		dev_err(&rpmsg_dev->dev, "Unable to init cdev\n");
		goto fail_cdev_init;
	}

	rpmsg_speed_dev->dev = device_create(rpmsg_speed_class, &rpmsg_dev->dev, rpmsg_speed_dev->devt, NULL, "rpmsg_speed");
	if (IS_ERR(rpmsg_speed_dev)) {
		dev_err(&rpmsg_dev->dev, "Failed to create device file entries\n");
		ret = PTR_ERR(rpmsg_speed_dev->dev);
		goto fail_device_create;
	}

	rpmsg_speed_dev->rpmsg_dev = rpmsg_dev;

	dev_set_drvdata(&rpmsg_dev->dev, rpmsg_speed_dev);
	dev_info(&rpmsg_dev->dev, "Device ready at /dev/rpmsg_speed\n");

	return 0;


fail_device_create:
	cdev_del(&rpmsg_speed_dev->cdev);
	
fail_cdev_init:
	idr_remove(&rpmsg_speed_minors, minor_obtained);
	
fail_idr_alloc:
	return ret;
}


static void rpmsg_speed_remove(struct rpmsg_channel *rpmsg_dev)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_remove\n");
	rpmsg_speed_dev = dev_get_drvdata(&rpmsg_dev->dev);

	device_destroy(rpmsg_speed_class, rpmsg_speed_dev->devt);
	cdev_del(&rpmsg_speed_dev->cdev);
	idr_remove(&rpmsg_speed_minors, MINOR(rpmsg_speed_dev->devt));
}


static int __init rpmsg_speed_init (void)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_init\n");
	
	mutex_init(&cb_mutex);
	if (cb_init(&cb1, CIRC_BUF_SIZE, sizeof(read_buf[0]))) {
            pr_err("Failed to init CB\n");
        }
        
	rpmsg_speed_class = class_create(THIS_MODULE, "rpmsg_speed");
	if (IS_ERR(rpmsg_speed_class)) {
		pr_err("Failed to create class\n");
		ret= PTR_ERR(rpmsg_speed_class);
		goto fail_class_create;
	}

	ret = alloc_chrdev_region(&rpmsg_speed_devt, 0, PRU_MAX_DEVICES, "rpmsg_speed");
	if (ret) {
		pr_err("Failed to allocate chrdev region\n");
		goto fail_alloc_chrdev_region;
	}

	ret = register_rpmsg_driver(&rpmsg_speed_driver);
	if (ret) {
		pr_err("Failed to register the driver on rpmsg bus\n");
		goto fail_register_rpmsg_driver;
	}

	return 0;

fail_register_rpmsg_driver:
	unregister_chrdev_region(rpmsg_speed_devt, PRU_MAX_DEVICES);
	
fail_alloc_chrdev_region:
	class_destroy(rpmsg_speed_class);
	
fail_class_create:
	return ret;
}


static void __exit rpmsg_speed_exit (void)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_exit\n");
	unregister_rpmsg_driver(&rpmsg_speed_driver);
	idr_destroy(&rpmsg_speed_minors);
	class_destroy(rpmsg_speed_class);
	unregister_chrdev_region(rpmsg_speed_devt, PRU_MAX_DEVICES);
	
	cb_free(&cb1);
	mutex_destroy(&cb_mutex);
}

//module_rpmsg_driver(rpmsg_speed_driver);

module_init(rpmsg_speed_init);
module_exit(rpmsg_speed_exit);
