/*
 * Source Modified by Per Hedlund < per.hedlund@gmail.com >
 * Based on the examples distributed by ZeekHuge
 *
 * Copyright (C) 2016 Zubeen Tolani <ZeekHuge - zeekhuge@gmail.com>
 *
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef RPMSG_SPEED_CB_H
#define RPMSG_SPEED_CB_H

#define PRU_MAX_DEVICES 4
#define WRITE_BUF_SIZE	8
#define READ_BUF_SIZE	10000
#define CIRC_BUF_SIZE	1000000

static struct class *rpmsg_speed_class;
static struct rpmsg_speed_dev *rpmsg_speed_dev;

static dev_t rpmsg_speed_devt;
static int minor_obtained;
static int ret;

static struct circular_buffer cb1;
static uint8_t read_buf[READ_BUF_SIZE];
static uint8_t write_buf[WRITE_BUF_SIZE];
//static uint8_t *rx_buf;

// The prototype functions for the character module
static int     rpmsg_speed_open(struct inode *, struct file *);
static int     rpmsg_speed_release(struct inode *, struct file *);
static ssize_t rpmsg_speed_read(struct file *, char __user *, size_t, loff_t *);
static ssize_t rpmsg_speed_write(struct file *, const char __user *, size_t, loff_t *);

// The prototype functions for the pru rpmsg driver
static void rpmsg_speed_cb(struct rpmsg_channel *, void *, int, void *, u32);
static int  rpmsg_speed_probe(struct rpmsg_channel *);
static void rpmsg_speed_remove(struct rpmsg_channel *);

// Local prototype functions
static void send_to_pru(struct rpmsg_speed_dev *rpmsg_speed_dev);
static int send_to_user(void *buf);

static const struct file_operations rpmsg_speed_fops = {
    .owner	 = THIS_MODULE,
	.open	 = rpmsg_speed_open,
	.write	 = rpmsg_speed_write,
	.read	 = rpmsg_speed_read,
	.release = rpmsg_speed_release,
};

static const struct rpmsg_device_id rpmsg_speed_id_table[] = {
	{ .name = "rpmsg-speed" },
	{ },
};

static struct rpmsg_driver rpmsg_speed_driver = {
	.drv.name	= KBUILD_MODNAME,
	.drv.owner	= THIS_MODULE,
	.id_table	= rpmsg_speed_id_table,
	.probe		= rpmsg_speed_probe,
	.callback	= rpmsg_speed_cb,
	.remove		= rpmsg_speed_remove,
};

struct rpmsg_speed_dev {
	struct rpmsg_channel *rpmsg_dev;
	struct device *dev;
	bool dev_lock;
	bool buf_lock;
	struct cdev cdev;
	dev_t devt;
};

#endif // RPMSG_SPEED_CB_H