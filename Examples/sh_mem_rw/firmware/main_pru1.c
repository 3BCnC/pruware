/* 
 * Copyright (c) 2017 Per Hedlund (per.hedlund@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <stdint.h>
#include <pru_cfg.h>
#include "resource_table_empty.h"
#include "sh_mem.h"

#define RB_SIZE 10

volatile register uint32_t __R30;

void main(void)
{
	// local variables
	volatile uint32_t gpio = 0x0002; // Mask for r30_1 (P8_46)
	uint8_t readBuffer[RB_SIZE];
	int i, j, delay_us;
	
	/* Clear SYSCFG[STANDBY_INIT] to enable OCP master port */
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;
	
	/* TODO: Create stop condition, else it will toggle indefinitely */
	while (1) {
		// reset led
		__R30 = 0;
		
		// check flag if data available
		if (getMutexFlag() == DATA_AVAIL) {
			
			// read data from shared memory
			readShMem(&readBuffer, sizeof(readBuffer));
		
			// blink led to validate data transfer
			for (i=1; i<RB_SIZE; i++) {
				__R30 ^= gpio;
				delay_us = 100000 * (int) readBuffer[i];
				
				// delay loop
				for (j=0; j<delay_us; j++) {
					__delay_cycles(200); // PRU clock = 200 MHz
				}
			}
		}
	}
	
	/* Halt the PRU core - shouldn't get here */
	//__halt();
}
