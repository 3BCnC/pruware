/* 
 * Copyright (c) 2017 Per Hedlund (per.hedlund@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <stdint.h>
#include <pru_cfg.h>
#include "resource_table_empty.h"
#include "sh_mem.h"

#define WB_SIZE 10

void main(void)
{
	// local variables
	uint8_t writeBuffer[WB_SIZE];
	int i;
	
	// fill write buffer
	for (i=0; i<WB_SIZE; i++) {
		writeBuffer[i] = (uint8_t) i+1;
	}
	
	// reset shared memory flag to unlocked
	setMutexFlag(UNLOCKED);

	/* Clear SYSCFG[STANDBY_INIT] to enable OCP master port */
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;

	/* TODO: Create stop condition, else it will toggle indefinitely */
	while (1) {
		
		// check flag if not locked 
		if (getMutexFlag() != LOCKED) {
			
			// write data to shared memory
			writeShMem(&writeBuffer, sizeof(writeBuffer));
			
			// wait 10 s
			__delay_cycles(2000000000);
		}
	}
	
	/* Halt the PRU core - shouldn't get here */
	//__halt();
}

