#! /bin/bash
 
 # Copyright (c) 2017 Per Hedlund (per.hedlund@gmail.com).
 # 
 # This program is free software: you can redistribute it and/or modify  
 # it under the terms of the GNU Lesser General Public License as   
 # published by the Free Software Foundation, version 3.
 #
 # This program is distributed in the hope that it will be useful, but 
 # WITHOUT ANY WARRANTY; without even the implied warranty of 
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 # Lesser General Lesser Public License for more details.
 #
 # You should have received a copy of the GNU Lesser General Public License
 # along with this program. If not, see <http://www.gnu.org/licenses/>.
 
HEADER=P8_
#PINS="27 28 29 39 40 41 42 43 44 45 46"
PINS="46"

echo "Moving the firmware to /lib/firmware..."
	cp gen/main_pru0_fw.out /lib/firmware/am335x-pru0-fw
	cp gen/main_pru1_fw.out /lib/firmware/am335x-pru1-fw
	
echo "Shutting down PRU0"
	echo "4a334000.pru0" > /sys/bus/platform/drivers/pru-rproc/unbind 2>/dev/null
echo "Shutting down PRU1"
	echo "4a338000.pru1"  > /sys/bus/platform/drivers/pru-rproc/unbind 2> /dev/null
	
echo "-Configuring pinmux"
	for PIN_NUMBER in $PINS
	do
		config-pin -a $HEADER$PIN_NUMBER pruout
		config-pin -q $HEADER$PIN_NUMBER
	done
	
echo "Booting PRU0"
	echo "4a334000.pru0" > /sys/bus/platform/drivers/pru-rproc/bind
echo "Booting PRU1"
	echo "4a338000.pru1" > /sys/bus/platform/drivers/pru-rproc/bind

echo "Done"