
###### sh_mem_rw ######
The example transfers data between the two PRUs via the shared memory. 
PRU0 writes data and PRU1 reads the data and blinks an LED (on pin P8_46) based on the data transferred.

To get the example working:

Compile the source (cd to sh_mem_rw) :
        
        $ cd firmware
        $ make
        $ ./deploy.sh

The LED should now be blinking.