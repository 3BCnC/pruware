/* 
 * Copyright (c) 2017 Per Hedlund (per.hedlund@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include "circularbuffer.h"

#define EXEC_TIME 1 // sec 

pthread_mutex_t lock;

// Producer thread
void *producer(void *arg) {

	struct circular_buffer *cb = (struct circular_buffer *) arg;
	int chunkSize = 10;
	int dataIn[chunkSize];
	int i;
	
	for (i=0; i<chunkSize; i++) {
		dataIn[i] = i;
    }
	
	// Produce 10 elements 1000 times/sec for EXEC_TIME sec to the buffer
	for (i=0; i<(EXEC_TIME * 1000); i++) {
		pthread_mutex_lock(&lock);
		//printf("P Locked\n");
		
		if (cb_push_chunk(cb, &dataIn, chunkSize)) {
            fprintf(stderr, "CB out of space\n");
        }
        
        pthread_mutex_unlock(&lock);
        //printf("P Unlocked\n");
        
        usleep(1000);
    }
    
   	pthread_exit(0);
}

// Consumer thread
void *consumer(void *arg) {
	
	struct circular_buffer *cb = (struct circular_buffer *) arg;
	int bufSize = 200;
	int dataOut[bufSize];
	int i = 0;
	int j = 0;
	
	// Consume all elements currently in the buffer 100 times/sec until buffer is empty
    usleep(10000);
    while (cb->count > 0) {
    	cb_print(cb);
    	
    	// Reset output buffer
    	memset(dataOut, 0, sizeof(dataOut));
    	/*for (j=0; j<bufSize; j++) {
			dataOut[j] = 0;
    	}*/

    	printf("Consumer loop %d\n", i);
    	pthread_mutex_lock(&lock);
    	//printf("C Locked\n");
    	
    	if (cb_pop_all(cb, &dataOut)) {
            fprintf(stderr, "CB empty\n");
        }
		
    	pthread_mutex_unlock(&lock);
    	//printf("C Unlocked\n");
		
		// Print output buffer
    	printf("dataOut: ");
    	for (j=0; j<bufSize; j++) {
			printf("%d ", dataOut[j]);
    	}
    	printf("\n");
    	
    	//cb_print(cb);
    	i++;
    	usleep(10000);
    } 
    
    pthread_exit(0);
}

int main(void)
{
	int size = BUFSIZE;
	struct circular_buffer cb1;
	pthread_t tidp, tidc;
	//pthread_attr_t attr; 
	
	if (cb_init(&cb1, BUFSIZE, sizeof(int))) {
            fprintf(stderr, "Failed to init CB\n");
        }
	
	cb_print(&cb1);
	
	if (pthread_mutex_init(&lock, NULL) != 0)
    {
        printf("\n Failed to init Mutex\n");
        return 1;
    }
    
    //pthread_attr_init(&attr);
    //pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	
	// Run producer and consumer threads in parallel
	pthread_create(&tidp, NULL, producer, &cb1);
	pthread_create(&tidc, NULL, consumer, &cb1);
	//pthread_create(&tidp, &attr, producer, &cb1);
	//pthread_create(&tidc, &attr, consumer, &cb1);
    
    //pthread_attr_destroy(&attr);
	
	// Wait for threads to finish
    pthread_join(tidc, NULL);
    pthread_join(tidp, NULL);
    
    pthread_mutex_destroy(&lock);
    
    cb_print(&cb1);
    cb_free(&cb1);
    
	printf("Done\n");
    return 0;
}

