#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>

#define BUFFER_LENGTH 8                ///< The buffer length (crude but fine)
static char receive[BUFFER_LENGTH];    ///< The receive buffer from the LKM

int main(){
	
   int ret, fd;
   printf("Starting device test code example...\n");
   fd = open("/dev/rpmsg_speed", O_RDWR);   // Open the device with read/write access
   if (fd < 0){
      perror("Failed to open the device...");
      return errno;
   }
   
   printf("Writing message 1 to the device to start test.\n");
   ret = write(fd, "1", 1); // Send the string to the LKM
   if (ret < 0){
      perror("Failed to write the message to the device.");
      return errno;
   }
   
   //sleep(5);
   printf("Press ENTER to stop the test...\n");
   getchar();
   
   printf("Writing message 0 to the device to stop test.\n");
   ret = write(fd, "0", 1); // Send the string to the LKM
   if (ret < 0){
      perror("Failed to write the message to the device.");
      return errno;
   }
   
   printf("Reading from the device...\n");
   ret = read(fd, receive, 1);        // Read the response from the LKM
   if (ret < 0){
      perror("Failed to read the message from the device.");
      return errno;
   }
   printf("The received message is: [%s]\n", receive);
   printf("The received message size is: [%d]\n", strlen(receive));
   printf("End of the program\n");
   return 0;
}
