/*
 * Source Modified by Per Hedlund < per.hedlund@gmail.com >
 * Based on the examples distributed by ZeekHuge
 *
 * Copyright (C) 2016 Zubeen Tolani <ZeekHuge - zeekhuge@gmail.com>
 *
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/kernel.h>
#include <linux/rpmsg.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/module.h>
#include <linux/kfifo.h>
#include <linux/uaccess.h>
#include <linux/poll.h>
#include <linux/delay.h>

#define PRU_MAX_DEVICES 4
#define WRITE_BUF_SIZE	8
#define READ_BUF_SIZE	8

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Per Hedlund <per.hedlund@gmail.com>");
MODULE_DESCRIPTION("An rpmsg speed test driver for the BBB");
MODULE_VERSION("0.1");

struct rpmsg_speed_dev {
	struct rpmsg_channel *rpmsg_dev;
	struct device *dev;
	bool dev_lock;
	bool buf_lock;
	struct cdev cdev;
	dev_t devt;
};

static struct class *rpmsg_speed_class;
static dev_t rpmsg_speed_devt;

static DEFINE_IDR(rpmsg_speed_minors);

static char read_buf[READ_BUF_SIZE];
static char write_buf[WRITE_BUF_SIZE] = "0";
static char send_buf[WRITE_BUF_SIZE] = "0";
static int run_test = 0;
static int msg_count = 0;
static int msg_len = 0;
static int msg_delay_ms = 1; // added delay to avoid CPU overload

// The prototype functions for the character module
static int     rpmsg_speed_open(struct inode *, struct file *);
static int     rpmsg_speed_release(struct inode *, struct file *);
static ssize_t rpmsg_speed_read(struct file *, char __user *, size_t, loff_t *);
static ssize_t rpmsg_speed_write(struct file *, const char __user *, size_t, loff_t *);

// The prototype functions for the pru rpmsg driver
static void rpmsg_speed_cb(struct rpmsg_channel *, void *, int, void *, u32);
static int  rpmsg_speed_probe(struct rpmsg_channel *);
static void rpmsg_speed_remove(struct rpmsg_channel *);


static const struct file_operations rpmsg_speed_fops = {
    .owner	 = THIS_MODULE,
	.open	 = rpmsg_speed_open,
	.write	 = rpmsg_speed_write,
	.read	 = rpmsg_speed_read,
	.release = rpmsg_speed_release,
};


static const struct rpmsg_device_id
	rpmsg_speed_id_table[] = {
		{ .name = "rpmsg-speed" },
		{ },
	};
	
MODULE_DEVICE_TABLE(rpmsg, rpmsg_speed_id_table);

static struct rpmsg_driver rpmsg_speed_driver = {
	.drv.name	= KBUILD_MODNAME,
	.drv.owner	= THIS_MODULE,
	.id_table	= rpmsg_speed_id_table,
	.probe		= rpmsg_speed_probe,
	.callback	= rpmsg_speed_cb,
	.remove		= rpmsg_speed_remove,
};

static struct rpmsg_speed_dev *rpmsg_speed_dev;
static int ret;
static int minor_obtained;

static void send_to_pru (struct rpmsg_speed_dev *rpmsg_speed_dev) {
	rpmsg_speed_dev->buf_lock = true;
	ret = rpmsg_send(rpmsg_speed_dev->rpmsg_dev, (void *)send_buf,
			 WRITE_BUF_SIZE*sizeof(char));
	if (ret) {
		dev_err(rpmsg_speed_dev->dev, "Transmission on rpmsg bus failed %d\n", ret);
	}
	rpmsg_speed_dev->buf_lock = false;
}	

static int rpmsg_speed_open(struct inode *inode,
					   struct file *filp)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_open\n");
	ret = -EACCES;
	rpmsg_speed_dev = container_of(inode->i_cdev,
				      struct rpmsg_speed_dev,
				      cdev);

	if (!rpmsg_speed_dev->dev_lock) {
		rpmsg_speed_dev->dev_lock = true;
		filp->private_data = rpmsg_speed_dev;
		ret = 0;
	}

	if (ret)
		dev_err(rpmsg_speed_dev->dev,
			"Failed to open already open device\n");

	return ret;
}


static ssize_t rpmsg_speed_write(struct file *filp,
						const char __user *buf,
						size_t count, loff_t *f_ops)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_write\n");
	rpmsg_speed_dev = filp->private_data;

	if (!rpmsg_speed_dev->buf_lock) {
		
		
		if (count > 1) {
			dev_err(rpmsg_speed_dev->dev, "Only 0 or 1 allowed, ie run test false/true\n");
			return -EINVAL;
		}
		
		ret = copy_from_user(write_buf, buf, count);
		if (ret) {
			dev_err(rpmsg_speed_dev->dev, "Failed to read %d chars from the user\n", ret);
			return -EFAULT;
		}
		
		run_test = write_buf[0] - '0';
		dev_info(rpmsg_speed_dev->dev, "Write message : %s\n", write_buf);
		send_to_pru(rpmsg_speed_dev);
		return count;
	}

	dev_err(rpmsg_speed_dev->dev, "Buffer is locked\n");
	return -EBUSY;
}


static ssize_t rpmsg_speed_read(struct file *filp,
						char __user *buf,
						size_t count, loff_t *f_ops)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_read\n");
	rpmsg_speed_dev = filp->private_data;
    
    if (!rpmsg_speed_dev->buf_lock) {
	    // copy_to_user has the format ( * to, *from, size) and returns 0 on success
	    sprintf(read_buf, "%d*%d", msg_count, msg_len);
	    ret = copy_to_user(buf, read_buf, strlen(read_buf));
	  
	    if (ret) {  
	    	dev_err(rpmsg_speed_dev->dev, "Failed to send %d chars to the user\n", ret);
	    	rpmsg_speed_dev->buf_lock = false;
	    	return -EFAULT; 
		}
		dev_info(rpmsg_speed_dev->dev, "Read message : %s\n", read_buf);
		return ret;
    }
    dev_err(rpmsg_speed_dev->dev, "Buffer is locked\n");
	return -EBUSY;
}


static int rpmsg_speed_release(struct inode *inode,
					      struct file *filp)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_release\n");
	rpmsg_speed_dev = container_of(inode->i_cdev,
				      struct rpmsg_speed_dev,
				      cdev);
	rpmsg_speed_dev->buf_lock = false;
	rpmsg_speed_dev->dev_lock = false;

	return 0;
}


static void rpmsg_speed_cb(struct rpmsg_channel *rpmsg_dev,
					  void *data , int len , void *priv,
					  u32 src )
{
	//printk(KERN_DEBUG "Enter rpmsg_speed_cb\n");
	rpmsg_speed_dev = dev_get_drvdata(&rpmsg_dev->dev);
	++msg_count;
	msg_len = len;
	sprintf(send_buf, "%d", msg_count);
	
	//sprintf(read_buf, "%s", data);
	//dev_info(&rpmsg_dev->dev, "Incoming message : %s\n", read_buf);
	if (run_test) {
		if (msg_delay_ms > 0){
			msleep(msg_delay_ms); 
		}
		send_to_pru(rpmsg_speed_dev);
	}
}


static int rpmsg_speed_probe(struct rpmsg_channel *rpmsg_dev)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_probe\n");
	dev_info(&rpmsg_dev->dev, "Chnl: 0x%x -> 0x%x\n", rpmsg_dev->src,
		 rpmsg_dev->dst);

	rpmsg_speed_dev = devm_kzalloc(&rpmsg_dev->dev, sizeof(*rpmsg_speed_dev),
				      GFP_KERNEL);
	if(!rpmsg_speed_dev)
		return -ENOMEM;


	minor_obtained = idr_alloc(&rpmsg_speed_minors,
				   rpmsg_speed_dev, 0, PRU_MAX_DEVICES,
				   GFP_KERNEL);

	if(minor_obtained < 0) {
		ret = minor_obtained;
		dev_err(&rpmsg_dev->dev, "Failed to get a minor number with return value %d\n",
			ret);
		goto fail_idr_alloc;
	}

	rpmsg_speed_dev->devt = MKDEV(MAJOR(rpmsg_speed_devt),
				     minor_obtained);

	cdev_init(&rpmsg_speed_dev->cdev, &rpmsg_speed_fops);
	rpmsg_speed_dev->cdev.owner = THIS_MODULE;
	ret = cdev_add(&rpmsg_speed_dev->cdev, rpmsg_speed_dev->devt,1);
	if (ret) {
		dev_err(&rpmsg_dev->dev, "Unable to init cdev\n");
		goto fail_cdev_init;
	}

	rpmsg_speed_dev->dev = device_create(rpmsg_speed_class,
					    &rpmsg_dev->dev,
					    rpmsg_speed_dev->devt, NULL, "rpmsg_speed");
	if (IS_ERR(rpmsg_speed_dev)) {
		dev_err(&rpmsg_dev->dev, "Failed to create device file entries\n");
		ret = PTR_ERR(rpmsg_speed_dev->dev);
		goto fail_device_create;
	}

	rpmsg_speed_dev->rpmsg_dev = rpmsg_dev;

	dev_set_drvdata(&rpmsg_dev->dev, rpmsg_speed_dev);
	dev_info(&rpmsg_dev->dev, "Device ready at /dev/rpmsg_speed\n");

	return 0;


fail_device_create:
	cdev_del(&rpmsg_speed_dev->cdev);
fail_cdev_init:
	idr_remove(&rpmsg_speed_minors, minor_obtained);
fail_idr_alloc:
	return ret;
}


static void rpmsg_speed_remove(struct rpmsg_channel *rpmsg_dev)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_remove\n");
	rpmsg_speed_dev = dev_get_drvdata(&rpmsg_dev->dev);

	device_destroy(rpmsg_speed_class, rpmsg_speed_dev->devt);
	cdev_del(&rpmsg_speed_dev->cdev);
	idr_remove(&rpmsg_speed_minors,
		   MINOR(rpmsg_speed_dev->devt));
}



static int __init rpmsg_speed_init (void)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_init\n");
	rpmsg_speed_class = class_create(THIS_MODULE, "rpmsg_speed");
	if (IS_ERR(rpmsg_speed_class))
	{
		pr_err("Failed to create class\n");
		ret= PTR_ERR(rpmsg_speed_class);
		goto fail_class_create;
	}

	ret = alloc_chrdev_region(&rpmsg_speed_devt, 0,
				  PRU_MAX_DEVICES, "rpmsg_speed");
	if (ret) {
		pr_err("Failed to allocate chrdev region\n");
		goto fail_alloc_chrdev_region;
	}

	ret = register_rpmsg_driver(&rpmsg_speed_driver);
	if (ret) {
		pr_err("Failed to register the driver on rpmsg bus\n");
		goto fail_register_rpmsg_driver;
	}

	return 0;

fail_register_rpmsg_driver:
	unregister_chrdev_region(rpmsg_speed_devt,
				 PRU_MAX_DEVICES);
fail_alloc_chrdev_region:
	class_destroy(rpmsg_speed_class);
fail_class_create:
	return ret;
}


static void __exit rpmsg_speed_exit (void)
{
	printk(KERN_DEBUG "Enter rpmsg_speed_exit\n");
	unregister_rpmsg_driver(&rpmsg_speed_driver);
	idr_destroy(&rpmsg_speed_minors);
	class_destroy(rpmsg_speed_class);
	unregister_chrdev_region(rpmsg_speed_devt,
				 PRU_MAX_DEVICES);
}


//module_rpmsg_driver(rpmsg_speed_driver);

module_init(rpmsg_speed_init);
module_exit(rpmsg_speed_exit);
