
###### rpmsg_speed
The example contains a kernel module module/rpmsg_speed.c and a firmware for pru-1 firmware/main_pru1.c. 

The module is to test the speed of the rpmsg interface between the ARM and PRU. 
The module when probed, creates a character device at /dev/rpmsg_speed. 
Writing a number (1 or 0) to this device will start/stop the test where messages are bounced between the PRU and ARM. The ARM sends 8 bytes and the PRU sends 496 bytes back.

To get the example working:

Compile the source (cd to rpmsg_speed) :
        
        $ make
        $ cd ../firmware
        $ ./deploy.sh
        $ cd ../module
        $ sudo insmod rpmsg_speed.ko
        $ cd ..
        $ ./build
        $ ./test

In case an error like "No rule to make target 'modules'." occurs when running the make file, there might be an issue with the paths. Try to add a symbolic link to your kernel source headers to the build and src folders.

        $ uname -r
        4.4.30-ti-r64
        
        $ rm /lib/modules/4.4.30-ti-r64/build
        $ rm /lib/modules/4.4.30-ti-r64/src
        $ ln -s /usr/src/linux-headers-4.4.30-ti-r64 /lib/modules/4.4.30-ti-r64/build
        $ ln -s /usr/src/linux-headers-4.4.30-ti-r64 /lib/modules/4.4.30-ti-r64/src