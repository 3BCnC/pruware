/* 
 * Copyright (c) 2017 Roger Isaksson (erogisa@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "resource_table_empty.h"
#include "spi_resources.h"
#include "sh_mem.h"

void main(){
	
	register uint16_t i = 0;
	register uint16_t j = 0;
	uint16_t miso_val = 0;					
	
	uint16_t samples[4][10] = {0}; //4 IMU chip reads consisting of 10 sensor channels each per sample
	
	// Reset shared memory flag to unlocked
	setMutexFlag(UNLOCKED);
	
	/* Clear SYSCFG[STANDBY_INIT] to enable OCP master port */
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;
	
	/* Configure GPI and GPO as Mode 0 (Direct Connect) */
	CT_CFG.GPCFG0 = 0x0000;

	// Set all the MPU CS pins to high
	for(i = 0; i < cs_array_nelems; i++){
		setCs(cs_array[i]);
	}

	/* Set Clock to Base State (Sourcing High)*/
	setSclk();

	// Clear the MOSI pin to low
	clearMosi();
	
	// Clear the FSYNC pin to low
	clearFsync();
	
	/* Initialize all the MPU's (CS-pin)*/
	for(i = 0; i < cs_array_nelems; i++){
		/*For every SPI transfer */
		for(j = 0; j < setup_cmds_nrows; j++){
			/*Do the Transfer */
			spiIO(&mpu_setup_commands[j], &miso_val, cs_array[i]);
		}
	}
	
	/* Start the DAQ loop */
	while (1) {
		/*For every MPU (CS-pin)*/
		for(i = 0; i < cs_array_nelems; i++){
			/*For every SPI transfer */
			for(j = 0; j < daq_cmds_nrows; j++){
				/*Do the Transfer */
				spiIO(&mpu_daq_commands[j], &miso_val, cs_array[i]);
				/*Check if there is a new magneto sample in the ST1 register of the Magneto*/
				if((j==0) && !(miso_val&0x0003)){
					/*Skip the rest of the Magneto reads*/
					j=7;
					continue;
				}
				
				const ByteOrder bo = (ByteOrder)((mpu_daq_commands[j][2] & 0xFF00) >> 8);
				const BusOperations ops = (BusOperations)(mpu_daq_commands[j][2] & 0x00FF);
				
				/* General reading/writing/specific sensor measurements */
				switch(ops){
						case WRITE:
							break;
						case READ:
							break;
						/*all specific sensor values*/
						default:
							/*replace the hi/lo byte value for the sample at index*/
							switch(bo){
								case HI_BYTE:
									samples[i][ops] = (samples[i][ops] & 0x00FF) | (miso_val << 8);
									break;
								case LO_BYTE:
									samples[i][ops] = (samples[i][ops] & 0xFF00) | (miso_val & 0x00FF);
									break;
								default:
									break;
							}
							break;
				}
			}
		}
		
		// Check shared memory flag if not locked 
		if (getMutexFlag() != LOCKED) {
			// write data to shared memory
			writeShMem(&samples, sizeof(samples));
			// wait 1 ms between memory writes
			//__delay_cycles(200000);
		}
	}
	
	/* Halt the PRU core - shouldn't get here */
	//__halt();
}
