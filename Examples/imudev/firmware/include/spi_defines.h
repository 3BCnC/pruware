/* 
 * Copyright (c) 2017 Roger Isaksson (erogisa@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// The BBB GPIO Bank Registers. 
#define GPIO_BANK0			(0x44E07000)
#define GPIO_BANK1			(0x4804C000)
#define GPIO_BANK2			(0x481AC000)
#define GPIO_BANK3			(0x481AF000)

// The BBB GPIO data registers. 
#define GPIO_SETDATAOUT	    (0x194)
#define GPIO_CLEARDATAOUT	(0x190)
#define GPIO_OE_ADDR 		(0x134)
#define GPIO_DATAOUT 		(0x13C)
#define GPIO_DATAIN 		(0x138)

// Convenience defines for setting and resetting registers
#define GPIO_BANK0_SET		(GPIO_BANK0 + GPIO_SETDATAOUT)
#define GPIO_BANK0_CLEAR	(GPIO_BANK0 + GPIO_CLEARDATAOUT)
#define GPIO_BANK0_DATAIN	(GPIO_BANK0 + GPIO_DATAIN)

#define GPIO_BANK1_SET		(GPIO_BANK1 + GPIO_SETDATAOUT)
#define GPIO_BANK1_CLEAR	(GPIO_BANK1 + GPIO_CLEARDATAOUT)
#define GPIO_BANK1_DATAIN	(GPIO_BANK1 + GPIO_DATAIN)

#define GPIO_BANK2_SET		(GPIO_BANK2 + GPIO_SETDATAOUT)
#define GPIO_BANK2_CLEAR	(GPIO_BANK2 + GPIO_CLEARDATAOUT)
#define GPIO_BANK2_DATAIN	(GPIO_BANK2 + GPIO_DATAIN)

#define GPIO_BANK3_SET		(GPIO_BANK3 + GPIO_SETDATAOUT)
#define GPIO_BANK3_CLEAR	(GPIO_BANK3 + GPIO_CLEARDATAOUT)
#define GPIO_BANK3_DATAIN	(GPIO_BANK3 + GPIO_DATAIN)

//Convenience macro for accessing registers
#define registerAccess32(x)	(*(volatile uint32_t *)(x))

// GPIO bit (DIO selection)
#define DIO(x)				(uint32_t)(1<<x)

// SPI Bus defines.

// CS_1  (P8_11 / GPIO1_13 / gpio1[13])  -> GPIO_BANK1
#define CS_1				(13)

// CS_2  (P8_12 / GPIO1_12 / gpio1[12])  -> GPIO_BANK1
#define CS_2				(12)

// CS_3  (P9_12 / GPIO1_28 / gpio1[28])  -> GPIO_BANK1
#define CS_3				(28)

// CS_4  (P9_15 / GPIO1_16 / gpio1[16])  -> GPIO_BANK1
#define CS_4				(16)

// SCLK  (P9_22 / UART2_RXD / gpio0[2])  -> GPIO_BANK0
#define SCLK				(2)

// MOSI  (P9_18 / I2C1_SDA / gpio0[4])  -> GPIO_BANK0
#define MOSI				(4)

// MISO  (P9_21 / UART2_TXD / gpio0[3])  -> GPIO_BANK0
#define MISO				(3)

// FSYNC  (P9_41 / GPIO3_20 / gpio3[20])  -> GPIO_BANK3
#define FSYNC				(20)	

// DEBUG1  (P9_13 / GPIO0_31 / gpio0[31])  -> GPIO_BANK0
#define DEBUG1			(31)	

// DEBUG2 (P9_14/ GPIO1_18 / gpio1[18])  -> GPIO_BANK1
#define DEBUG2			(18)	

// SCLK delay cycles (half period)
#define SCLK_DELAY      (80) // 1 MHz (>400ns high/low period)
//#define SCLK_DELAY      (5) // 20 MHz
