/* 
 * Copyright (c) 2017 Roger Isaksson (erogisa@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <stdint.h>
#include "mpu_9250_defines.h"

typedef enum {
	NO_DELAY,
	DELAY200MS,
	I2CDELAY,
} Delays;

typedef enum {
	READ_MAG_X = 0,
	READ_MAG_Y = 1,
	READ_MAG_Z = 2,
	READ_ACC_X = 3,
	READ_ACC_Y = 4,
	READ_ACC_Z = 5,
	READ_GYR_X = 6,
	READ_GYR_Y = 7,
	READ_GYR_Z = 8,
	READ_TEMP  = 9,
	WRITE, //Write
	READ // Read
} BusOperations;

typedef enum{
	LO_BYTE,
	HI_BYTE
} ByteOrder;

/* Array definition [command, delay_time, R/W]*/

static const uint16_t mpu_setup_commands[][3] = {
									/*Accelerometer and Gyro Setup SPI commands*/
									{bytesToShort(MPUREG_PWR_MGMT_1, 0x80),DELAY200MS,WRITE}, // Reset
									{bytesToShort(MPUREG_PWR_MGMT_1, 0x01),DELAY200MS,WRITE}, // Wake up, auto select clock
									{bytesToShort(MPUREG_PWR_MGMT_2, 0x00),DELAY200MS,WRITE}, // Enable acc and gyro
									{bytesToShort(MPUREG_INT_ENABLE, 0x00),NO_DELAY,WRITE}, // Interrupt disable
									{bytesToShort(MPUREG_FIFO_EN, 0x00),NO_DELAY,WRITE}, // Disable FIFO
									{bytesToShort(MPUREG_SMPLERT_DIV, 0x00),NO_DELAY,WRITE}, // No sample rate divider
									{bytesToShort(MPUREG_CONFIG, 0x01),NO_DELAY,WRITE}, // Gyro DLPF cutoff 184 Hz and Fs 1 kHz (temp cutoff 188 Hz)
									{bytesToShort(MPUREG_GYRO_CONFIG, 0x00),NO_DELAY,WRITE}, // Gyro scale +250dps and enable DLFP  
									{bytesToShort(MPUREG_ACCEL_CONFIG_1, 0x00),NO_DELAY,WRITE}, // Acc scale +2g
									{bytesToShort(MPUREG_ACCEL_CONFIG_2, 0x00),NO_DELAY,WRITE}, // Enable Acc DLPF with cuttoff 218 Hz and Fs 1 kHz
									
									/*Magnetometer Setup I2C commands*/
									{bytesToShort(MPUREG_I2C_USER_CTRL, 0x20),DELAY200MS,WRITE}, // Enable I2C master interface
									{bytesToShort(MPUREG_I2C_MST_CTRL, 0x0D),NO_DELAY,WRITE}, // I2C clock 400 kHz (fast mode)
									
									{bytesToShort(I2C_SLV0_ADDR, AK8963_ADDRESS),NO_DELAY,WRITE}, 
									{bytesToShort(I2C_SLV0_REG, AK8963_CNTL2),NO_DELAY,WRITE}, 
									{bytesToShort(I2C_SLV0_DO, 0x01),NO_DELAY,WRITE}, // Reset registers
									{bytesToShort(I2C_SLV0_CTRL, 0x81),DELAY200MS,WRITE}, 
									
									{bytesToShort(I2C_SLV0_ADDR, AK8963_ADDRESS),NO_DELAY,WRITE}, 
									{bytesToShort(I2C_SLV0_REG, AK8963_CNTL1),NO_DELAY,WRITE}, 
									{bytesToShort(I2C_SLV0_DO, 0x16),NO_DELAY,WRITE}, // 16 bit, start cont meas 2 (100 Hz)
									{bytesToShort(I2C_SLV0_CTRL, 0x81),DELAY200MS,WRITE},
									
									/*Magnetometer Data SPI to I2C commands (I2C data is stored in SPI data registers)*/
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE}, // Enable read
									{bytesToShort(I2C_SLV0_REG, AK8963_ST1),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_DO, 0x00),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, 0x88),I2CDELAY,WRITE} // Read 8 bytes: Reg ST1, 6*Meas and ST2
									};
									
static const uint16_t setup_cmds_nrows = LEN(mpu_setup_commands);

static const uint16_t mpu_daq_commands[][3] = { 
									/*I2C Magnetometer Data SPI to I2C commands*/
									/*Not needed!
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_ST1),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ+8),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,READ},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_XOUT_L),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_MAG_X)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_XOUT_H),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_MAG_X)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_YOUT_L),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_MAG_Y)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_YOUT_H),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_MAG_Y)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_ZOUT_L),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_MAG_Z)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_ZOUT_H),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_MAG_Z)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_ST2),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG),0),NO_DELAY,READ},*/
									
									/*Magnetometer Data SPI commands*/
									{bytesToShort((MPU_READ_FLAG | (EXT_SENS_DATA_00+0)), 0),NO_DELAY,READ},
									{bytesToShort((MPU_READ_FLAG | (EXT_SENS_DATA_00+1)), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_MAG_X)},
									{bytesToShort((MPU_READ_FLAG | (EXT_SENS_DATA_00+2)), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_MAG_X)},
									{bytesToShort((MPU_READ_FLAG | (EXT_SENS_DATA_00+3)), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_MAG_Y)},
									{bytesToShort((MPU_READ_FLAG | (EXT_SENS_DATA_00+4)), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_MAG_Y)},
									{bytesToShort((MPU_READ_FLAG | (EXT_SENS_DATA_00+5)), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_MAG_Z)},
									{bytesToShort((MPU_READ_FLAG | (EXT_SENS_DATA_00+6)), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_MAG_Z)},
									{bytesToShort((MPU_READ_FLAG | (EXT_SENS_DATA_00+7)), 0),NO_DELAY,READ},
									
									/*Accelerometer Data SPI commands*/
									{bytesToShort((MPU_READ_FLAG | ACCEL_XOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_ACC_X)},
									{bytesToShort((MPU_READ_FLAG | ACCEL_XOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_ACC_X)},
									{bytesToShort((MPU_READ_FLAG | ACCEL_YOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_ACC_Y)},
									{bytesToShort((MPU_READ_FLAG | ACCEL_YOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_ACC_Y)},
									{bytesToShort((MPU_READ_FLAG | ACCEL_ZOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_ACC_Z)},
									{bytesToShort((MPU_READ_FLAG | ACCEL_ZOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_ACC_Z)},
									
									/*Gyro Data SPI commands*/
									{bytesToShort((MPU_READ_FLAG | GYRO_XOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_GYR_X)},
									{bytesToShort((MPU_READ_FLAG | GYRO_XOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_GYR_X)},
									{bytesToShort((MPU_READ_FLAG | GYRO_YOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_GYR_Y)},
									{bytesToShort((MPU_READ_FLAG | GYRO_YOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_GYR_Y)},
									{bytesToShort((MPU_READ_FLAG | GYRO_ZOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_GYR_Z)},
									{bytesToShort((MPU_READ_FLAG | GYRO_ZOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_GYR_Z)},
									
									/*Temperature Data SPI commands*/
									{bytesToShort((MPU_READ_FLAG | TEMP_OUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_TEMP)},
									{bytesToShort((MPU_READ_FLAG | TEMP_OUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_TEMP)},
									};

static const uint16_t daq_cmds_nrows = LEN(mpu_daq_commands);
