/* 
 * Copyright (c) 2017 Per Hedlund (per.hedlund@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_MEM_H
#define SH_MEM_H

#include <stdint.h>

// shared memory address offset
#define PRU_SHARED_MEM_ADDR 0x00010000

// mutex flag enum
typedef enum {
	UNLOCKED	= 0,
	LOCKED		= 1,
	DATA_AVAIL	= 2,
} MutexFlag_t;

// prototype functions
MutexFlag_t getMutexFlag(void);
void setMutexFlag(MutexFlag_t mf);
void writeShMem(const void *buffer, size_t size);
void readShMem(void *buffer, size_t size);

#endif /* SH_MEM_H */

