/* 
 * Copyright (c) 2017 Per Hedlund (per.hedlund@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/slab.h>
#include "circularbuffer.h"

int cb_init(struct circular_buffer *cb, size_t capacity, size_t sz)
{
    cb->buffer = kzalloc(capacity * sz, GFP_KERNEL);
    if(cb->buffer == NULL) {
        return -1;  // quit with an error
    }
    cb->buffer_end = (char *) cb->buffer + capacity * sz;
    cb->capacity = capacity;
    cb->count = 0;
    cb->sz = sz;
    cb->head = cb->buffer;
    cb->tail = cb->buffer;
    return 0;
}

void cb_free(struct circular_buffer *cb)
{
    kfree(cb->buffer);
    // clear out other fields too, just to be safe
}

int cb_push(struct circular_buffer *cb, const void *item)
{
    if(cb->count == cb->capacity) {
        return -1;  // quit with an error
    }
    memcpy(cb->head, item, cb->sz);
    cb->head = (char *) cb->head + cb->sz;
    if(cb->head == cb->buffer_end) {
        cb->head = cb->buffer;
    }
    cb->count++;
    return 0;
}

int cb_pop(struct circular_buffer *cb, void *item)
{
    if(cb->count == 0) {
        return -1;  // quit with an error
    }
    memcpy(item, cb->tail, cb->sz);
    cb->tail = (char *) cb->tail + cb->sz;
    if(cb->tail == cb->buffer_end) {
        cb->tail = cb->buffer;
    }
    cb->count--;
    return 0;
}

int cb_push_chunk(struct circular_buffer *cb, const void *items, size_t len)
{
	//printf("Enter cb_push_chunk\n");
	int i, ret;
	
    if((cb->count + len) >= cb->capacity) {
        return -ENOBUFS; 
    }
    for (i=0; i<len; i++) {
    	ret = cb_push(cb, items+(i*cb->sz));
    	if (ret > 0) {
    		return ret;
    	}
    }
    return 0;
}

int cb_pop_all(struct circular_buffer *cb, void *items)
{
	//printf("Enter cb_pop_all\n");
	int i, ret;
	int len = cb->count;
	
    if(len == 0){
        return -ENODATA; 
    }
    for (i=0; i<len; i++) {
    	ret = cb_pop(cb, items+(i*cb->sz));
    	if (ret > 0){
    		return ret;
    	}
    }
    return 0;
}

int cb_get_count(struct circular_buffer *cb)
{
    return (int) cb->count;
}

void cb_print(struct circular_buffer *cb)
{
	//printk(KERN_DEBUG"cb->head = %d\n", (int) cb->head);
    //printk(KERN_DEBUG"cb->tail = %d\n", (int) cb->tail);
    printk(KERN_DEBUG"cb->count = %d\n", (int) cb->count);
    //printk(KERN_DEBUG"cb->sz = %d\n", (int) cb->sz);
}
