/*
 * Source Modified by Per Hedlund < per.hedlund@gmail.com >
 * Based on the examples distributed by ZeekHuge
 *
 * Copyright (C) 2016 Zubeen Tolani <ZeekHuge - zeekhuge@gmail.com>
 *
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef IMUDEV_MOD_H
#define IMUDEV_MOD_H

#define PRU_MAX_DEVICES 4
#define WRITE_BUF_SIZE	8
#define READ_BUF_SIZE	10000
#define CIRC_BUF_SIZE	1000000

#define DEVICE_NAME "imudev"    	// will appear at /dev/imudev
#define CLASS_NAME  "bbbimu"
#define CHAN_NAME	"rpmsg-imudev"	// must match name in PRU firmware

static struct class *imudev_class;
static struct imudev_dev *imudev_devp;

static dev_t imudev_devt;
static int minor_obtained;
static int ret;
static int pop_count;

static struct circular_buffer cb1;
static uint8_t read_buf[READ_BUF_SIZE];
static uint8_t write_buf[WRITE_BUF_SIZE];
//static uint8_t *rx_buf;

// The prototype functions for the character module
static int     imudev_open(struct inode *, struct file *);
static int     imudev_release(struct inode *, struct file *);
static ssize_t imudev_read(struct file *, char __user *, size_t, loff_t *);
static ssize_t imudev_write(struct file *, const char __user *, size_t, loff_t *);

// The prototype functions for the pru rpmsg driver
static int imudev_cb(struct rpmsg_device *, void *, int, void *, u32);
static int  imudev_probe(struct rpmsg_device *);
static void imudev_remove(struct rpmsg_device *);

// Local prototype functions
static void send_to_pru(struct imudev_dev *devp);
static int send_to_user(struct imudev_dev *devp, void *buf);

static const struct file_operations imudev_fops = {
    .owner	 = THIS_MODULE,
	.open	 = imudev_open,
	.write	 = imudev_write,
	.read	 = imudev_read,
	.release = imudev_release,
};

static const struct rpmsg_device_id imudev_id_table[] = {
	{ .name = CHAN_NAME }, 
	{ },
};

static struct rpmsg_driver imudev_driver = {
	.drv.name	= KBUILD_MODNAME,
	.drv.owner	= THIS_MODULE,
	.id_table	= imudev_id_table,
	.probe		= imudev_probe,
	.callback	= imudev_cb,
	.remove		= imudev_remove,
};

struct imudev_dev {
	struct rpmsg_device *rpmsg_dev;
	struct device *dev;
	bool dev_lock;
	bool buf_lock;
	struct cdev cdev;
	dev_t devt;
};

#endif // IMUDEV_MOD_H