/* 
 * Copyright (c) 2017 Per Hedlund (per.hedlund@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

struct circular_buffer
{
    void *buffer;     // data buffer
    void *buffer_end; // end of data buffer
    size_t capacity;  // maximum number of items in the buffer
    size_t count;     // number of items in the buffer
    size_t sz;        // size of each item in the buffer
    void *head;       // pointer to head
    void *tail;       // pointer to tail
};

// Prototype functions
int cb_init(struct circular_buffer *cb, size_t capacity, size_t sz);
void cb_free(struct circular_buffer *cb);
int cb_push(struct circular_buffer *cb, const void *item);
int cb_pop(struct circular_buffer *cb, void *item);
int cb_push_chunk(struct circular_buffer *cb, const void *items, size_t len);
int cb_pop_all(struct circular_buffer *cb, void *items);
void cb_print(struct circular_buffer *cb);
int cb_get_count(struct circular_buffer *cb);

#endif // CIRCULARBUFFER_H