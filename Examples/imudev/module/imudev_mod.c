/*
 * Source Modified by Per Hedlund < per.hedlund@gmail.com >
 * Based on the examples distributed by ZeekHuge
 *
 * Copyright (C) 2016 Zubeen Tolani <ZeekHuge - zeekhuge@gmail.com>
 *
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/mutex.h>
#include <linux/kernel.h>
#include <linux/rpmsg.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/module.h>
#include <linux/kfifo.h>
#include <linux/uaccess.h>
#include <linux/poll.h>
#include <linux/delay.h>

#include "circularbuffer.h"
#include "imudev_mod.h"

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Per Hedlund <per.hedlund@gmail.com>");
MODULE_DESCRIPTION("Quad IMU device driver for BBB, receiving sensor data from PRU via RPMSG");
MODULE_VERSION("0.1");

MODULE_DEVICE_TABLE(rpmsg, imudev_id_table);
static DEFINE_IDR(imudev_minors);
static DEFINE_MUTEX(cb_mutex); // Mutex for circular buffer

static void send_to_pru(struct imudev_dev *devp) 
{
	devp->buf_lock = true;
	ret = rpmsg_send(devp->rpmsg_dev->ept, write_buf, sizeof(write_buf));
	if (ret) {
		dev_err(devp->dev, "Transmission on rpmsg bus failed %d\n", ret);
	}
	devp->buf_lock = false;
}	

static int send_to_user(struct imudev_dev *devp, void *buf) 
{
	// reset read buffer
    memset(read_buf, 0, sizeof(read_buf));
    
    mutex_lock(&cb_mutex);
    //cb_print(&cb1);
    
    // first add the circular buffer count to the read buffer
    pop_count = cb_get_count(&cb1);
    memcpy(&read_buf[0], (void *) &pop_count, sizeof(pop_count));
    
    // then append the circular buffer data to the read buffer
	ret = cb_pop_all(&cb1, &read_buf[sizeof(pop_count)]);
	//ret = cb_pop_all(&cb1, rx_buf);
	
	//cb_print(&cb1);
	mutex_unlock(&cb_mutex);
	
    if (ret) {  
    	dev_err(devp->dev, "CB empty. Err: %d\n", ret);
    	return -EFAULT; 
	}
	
	// copy_to_user has the format (*to, *from, size) and returns 0 on success
	ret = copy_to_user(buf, &read_buf, sizeof(read_buf));
	//ret = copy_to_user(buf, rx_buf, sizeof(read_buf));
	
	if (ret) {  
    	dev_err(devp->dev, "Failed to send data to the user. Err: %d\n", ret);
    	return -EFAULT; 
	}
	return ret;
}

static int imudev_open(struct inode *inode, struct file *filp)
{
	printk(KERN_DEBUG "Enter imudev_open\n");
	ret = -EACCES;
	imudev_devp = container_of(inode->i_cdev, struct imudev_dev, cdev);
	/*
	if (!rx_buf) {
		rx_buf = kmalloc(READ_BUF_SIZE, GFP_KERNEL);
		if (!rx_buf) {
			dev_err(imudev_dev->dev, "Failed to allocate rx_buf\n");
			ret = -ENOMEM;
		}
	}
	*/
	if (!imudev_devp->dev_lock) {
		imudev_devp->dev_lock = true;
		filp->private_data = imudev_devp;
		ret = 0;
	}

	if (ret) {
		dev_err(imudev_devp->dev, "Failed to open already open device\n");
	}
	
	return ret;
}

static int imudev_release(struct inode *inode, struct file *filp)
{
	printk(KERN_DEBUG "Enter imudev_release\n");
	imudev_devp = container_of(inode->i_cdev, struct imudev_dev, cdev);
	imudev_devp->buf_lock = false;
	imudev_devp->dev_lock = false;
	/*
	kfree(rx_buf);
	rx_buf = NULL;
	*/
	return 0;
}

static ssize_t imudev_read(struct file *filp, char __user *buf, size_t count, loff_t *f_ops)
{
	//printk(KERN_DEBUG "Enter imudev_read\n");
	imudev_devp = filp->private_data;
    
    if (!imudev_devp->buf_lock) {
		ret = send_to_user(imudev_devp, buf);
		
		if(ret) {
			imudev_devp->buf_lock = false;
		}
		return ret;
    }
    dev_err(imudev_devp->dev, "Buffer is locked\n");
	return -EBUSY;
}

static ssize_t imudev_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_ops)
{
	printk(KERN_DEBUG "Enter imudev_write\n");
	imudev_devp = filp->private_data;
	
	if (!imudev_devp->buf_lock) {
		
		if (count > 1) {
			dev_err(imudev_devp->dev, "Only 0 or 1 allowed, ie stop/start sampling\n");
			return -EINVAL;
		}
		
		ret = copy_from_user(write_buf, buf, count);
		if (ret) {
			dev_err(imudev_devp->dev, "Failed to read %d data from the user\n", ret);
			return -EFAULT;
		}
		
		//dev_info(imudev_dev->dev, "Write message : %s\n", write_buf);
		dev_info(imudev_devp->dev, "Write message : %d\n", write_buf[0]);
		send_to_pru(imudev_devp); // Initiate test
		
		return count;
	}

	dev_err(imudev_devp->dev, "Buffer is locked\n");
	return -EBUSY;
}

static int imudev_cb(struct rpmsg_device *rpmsg_dev, void *data , int len , void *priv, u32 src)
{
	//printk(KERN_DEBUG "Enter imudev_cb\n");
	imudev_devp = dev_get_drvdata(&rpmsg_dev->dev);
	//dev_info(&rpmsg_dev->dev, "Incoming message : %s\n", (char*) data);
	
	mutex_lock(&cb_mutex);
	ret = cb_push_chunk(&cb1, data, len);
	mutex_unlock(&cb_mutex);
	
	if (ret) {
        dev_err(imudev_devp->dev, "Out of space in CB. Err: %d\n", ret);
    }
    
    return 0;
}

static int imudev_probe(struct rpmsg_device *rpmsg_dev)
{
	printk(KERN_DEBUG "Enter imudev_probe\n");
	dev_info(&rpmsg_dev->dev, "Chnl: 0x%x -> 0x%x\n", rpmsg_dev->src, rpmsg_dev->dst);

	imudev_devp = devm_kzalloc(&rpmsg_dev->dev, sizeof(*imudev_devp), GFP_KERNEL);
	if(!imudev_devp)
		return -ENOMEM;


	minor_obtained = idr_alloc(&imudev_minors, imudev_devp, 0, PRU_MAX_DEVICES, GFP_KERNEL);

	if(minor_obtained < 0) {
		ret = minor_obtained;
		dev_err(&rpmsg_dev->dev, "Failed to get a minor number with return value %d\n", ret);
		goto fail_idr_alloc;
	}

	imudev_devp->devt = MKDEV(MAJOR(imudev_devt), minor_obtained);

	cdev_init(&imudev_devp->cdev, &imudev_fops);
	imudev_devp->cdev.owner = THIS_MODULE;
	ret = cdev_add(&imudev_devp->cdev, imudev_devp->devt,1);
	if (ret) {
		dev_err(&rpmsg_dev->dev, "Unable to init cdev\n");
		goto fail_cdev_init;
	}

	imudev_devp->dev = device_create(imudev_class, &rpmsg_dev->dev, imudev_devp->devt, NULL, DEVICE_NAME);
	if (IS_ERR(imudev_devp)) {
		dev_err(&rpmsg_dev->dev, "Failed to create device file entries\n");
		ret = PTR_ERR(imudev_devp->dev);
		goto fail_device_create;
	}

	imudev_devp->rpmsg_dev = rpmsg_dev;

	dev_set_drvdata(&rpmsg_dev->dev, imudev_devp);
	dev_info(&rpmsg_dev->dev, "Device ready at /dev/imudev\n");

	return 0;


fail_device_create:
	cdev_del(&imudev_devp->cdev);
	
fail_cdev_init:
	idr_remove(&imudev_minors, minor_obtained);
	
fail_idr_alloc:
	return ret;
}

static void imudev_remove(struct rpmsg_device *rpmsg_dev)
{
	printk(KERN_DEBUG "Enter imudev_remove\n");
	imudev_devp = dev_get_drvdata(&rpmsg_dev->dev);

	device_destroy(imudev_class, imudev_devp->devt);
	cdev_del(&imudev_devp->cdev);
	idr_remove(&imudev_minors, MINOR(imudev_devp->devt));
}

static int __init imudev_init (void)
{
	printk(KERN_DEBUG "Enter imudev_init\n");
	
	mutex_init(&cb_mutex);
	if (cb_init(&cb1, CIRC_BUF_SIZE, sizeof(read_buf[0]))) {
            pr_err("Failed to init CB\n");
        }
        
	imudev_class = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(imudev_class)) {
		pr_err("Failed to create class\n");
		ret= PTR_ERR(imudev_class);
		goto fail_class_create;
	}

	ret = alloc_chrdev_region(&imudev_devt, 0, PRU_MAX_DEVICES, DEVICE_NAME);
	if (ret) {
		pr_err("Failed to allocate chrdev region\n");
		goto fail_alloc_chrdev_region;
	}

	ret = register_rpmsg_driver(&imudev_driver);
	if (ret) {
		pr_err("Failed to register the driver on rpmsg bus\n");
		goto fail_register_rpmsg_driver;
	}

	return 0;

fail_register_rpmsg_driver:
	unregister_chrdev_region(imudev_devt, PRU_MAX_DEVICES);
	
fail_alloc_chrdev_region:
	class_destroy(imudev_class);
	
fail_class_create:
	return ret;
}

static void __exit imudev_exit (void)
{
	printk(KERN_DEBUG "Enter imudev_exit\n");
	unregister_rpmsg_driver(&imudev_driver);
	idr_destroy(&imudev_minors);
	class_destroy(imudev_class);
	unregister_chrdev_region(imudev_devt, PRU_MAX_DEVICES);
	
	cb_free(&cb1);
	mutex_destroy(&cb_mutex);
}

//module_rpmsg_driver(imudev_driver);

module_init(imudev_init);
module_exit(imudev_exit);
