/* 
 * Copyright (c) 2017 Per Hedlund (per.hedlund@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#define EXEC_TIME 1 // sec
#define BUFFER_LENGTH 10000

static uint8_t receive[BUFFER_LENGTH]; // The receive buffer from the LKM
static uint8_t send[8]; // The send buffer to the LKM

int main() {
	
   int ret, fd, i, di, recSz;
   char recItm;
   int bu = 0;
   
   printf("Starting device test code example.\n");
   fd = open("/dev/imudev", O_RDWR); // Open the device with read/write access
   if (fd < 0){
      perror("Failed to open the device.");
      return errno;
   }
   
   printf("Writing message 1 to the device to start sampling.\n");
   send[0] = 1;
   ret = write(fd, &send, 1);
   //ret = write(fd, "1", 1); // Send the string to the LKM
   if (ret < 0){
      perror("Failed to write the message to the device.");
      return errno;
   }
   
   printf("Reading from the device...\n");
   // Read all elements currently in the buffer 10 times/sec for EXEC_TIME sec
   for (i=0; i<(EXEC_TIME*10); i++) {
      usleep(100000);
      ret = read(fd, &receive[0], BUFFER_LENGTH); // Read the response from the LKM
      if (ret < 0){
         perror("Failed to read the message from the device.");
         return errno;
      }
      
      // convert first 4 bytes of uint8 to int
      recSz = receive[0] + (receive[1] << 8) + (receive[2] << 16) + (receive[3] << 24);
      //recSz = strlen((char *) receive); // not all bytes are counted for binary data
      //recSz = sizeof(receive); // always returns the total size
      
      if (recSz < 800) {
         bu++;
         //printf("Buffer underrun\n");
      }
      
      di = sizeof(recSz)+i;
      recItm = receive[di];
      //printf("Received message (element %d): [%c]\n", i, recItm); // print ascii char
      printf("Received message (element %d): [%d]\n", di, recItm); // print decimal
      printf("Received message size: [%d]\n", recSz);
   
      // Reset buffer
      memset(receive, 0, sizeof(receive));
   }
   
   printf("Writing message 0 to the device to stop sampling.\n");
   send[0] = 0;
   ret = write(fd, &send, 1);
   //ret = write(fd, "0", 1); // Send the string to the LKM
   if (ret < 0){
      perror("Failed to write the message to the device.");
      return errno;
   }

   //printf("Last received message (first element): [%c]\n", recItm);
   //printf("Last received message size: [%d]\n", recSz);
   printf("Buffer underrun %d times.\n", bu);
   printf("Test done.\n");
   
   return 0;
}
