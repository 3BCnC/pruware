/*
 * Source Modified by Roger Isaksson < erogisa - erogisa@gmail.com >
 * Based on the examples distributed by TI
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *	* Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 *
 *	* Redistributions in binary form must reproduce the above copyright
 *	  notice, this list of conditions and the following disclaimer in the
 *	  documentation and/or other materials provided with the
 *	  distribution.
 *
 *	* Neither the name of Texas Instruments Incorporated nor the names of
 *	  its contributors may be used to endorse or promote products derived
 *	  from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>
#include <pru_cfg.h>
#include "resource_table_empty.h"

// The BBB GPIO Bank Registers. 
#define GPIO_BANK0			(0x44E07000)
#define GPIO_BANK1			(0x4804C000)
#define GPIO_BANK2			(0x481AC000)
#define GPIO_BANK3			(0x481AF000)

// The BBB GPIO data registers. 
#define GPIO_SETDATAOUT	(0x194)
#define GPIO_CLEARDATAOUT	(0x190)
#define GPIO_OE_ADDR 		(0x134)
#define GPIO_DATAOUT 		(0x13C)
#define GPIO_DATAIN 		(0x138)

// Convenience defines for setting and resetting registers
#define GPIO_BANK1_SET		(GPIO_BANK1 + GPIO_SETDATAOUT)
#define GPIO_BANK1_CLEAR	(GPIO_BANK1 + GPIO_CLEARDATAOUT)

//Convenience macro for accessing registers
#define registerAccess32(x)	(*(volatile register uint32_t *)(x))

// GPIO bit (DIO selection)
#define DIO(x)				(1<<x)

// Get the nr of elements in an array
#define arrayLength(x)		(sizeof(x) / sizeof((x)[0]))

// LED positions in the 32-bit bank 
#define USR0_LED			(21)
#define USR1_LED			(22)
#define USR2_LED			(23)
#define USR3_LED			(24)

void main(void){
	uint32_t leds[4] = 	{USR0_LED,
					  USR1_LED,
					  USR2_LED,
					  USR3_LED};
					
	uint8_t ledsize = arrayLength(leds);
	uint8_t ledindex = 0;
	
	/* Clear SYSCFG[STANDBY_INIT] to enable OCP master port */
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;

	while (1) {
		registerAccess32(GPIO_BANK1_SET) 		= DIO(leds[ledindex]);
		__delay_cycles(10000000);
		registerAccess32(GPIO_BANK1_CLEAR) 	= DIO(leds[ledindex]);
		__delay_cycles(10000000);
		ledindex++;
		if (ledindex > ledsize)
			ledindex = 0;
	}
}
