#! /bin/bash
 
 # Copyright (c) 2017 Roger Isaksson (erogisa@gmail.com).
 # 
 # This program is free software: you can redistribute it and/or modify  
 # it under the terms of the GNU Lesser General Public License as   
 # published by the Free Software Foundation, version 3.
 #
 # This program is distributed in the hope that it will be useful, but 
 # WITHOUT ANY WARRANTY; without even the implied warranty of 
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 # Lesser General Lesser Public License for more details.
 #
 # You should have received a copy of the GNU Lesser General Public License
 # along with this program. If not, see <http://www.gnu.org/licenses/>.
 
PRU_CORE=0

echo "Making the binaries..."
	cd source
	make clean
	make

echo "Moving the firmware to /lib/firmware..."
	cp gen/*.out /lib/firmware/am335x-pru$PRU_CORE-fw
	
echo "Shutting down the PRU with the old firmware..."
	if [ $PRU_CORE -eq 0 ]
	then
		echo "Shutting down PRU0"
		echo "4a334000.pru0" > /sys/bus/platform/drivers/pru-rproc/unbind 2>/dev/null
	else
		echo "Shutting down PRU1"
		echo "4a338000.pru1"  > /sys/bus/platform/drivers/pru-rproc/unbind 2> /dev/null
	fi

echo "-Configuring CS1"
	config-pin -a P8_11 hi+
	config-pin -q P8_11
	
echo "-Configuring CS2"
	config-pin -a P8_12 hi+
	config-pin -q P8_12
	
echo "-Configuring CS3"
	config-pin -a P9_12 hi+
	config-pin -q P9_12
	
echo "-Configuring DEBUG1" 
	config-pin -a P9_13 hi-
	config-pin -q P9_13
	
echo "-Configuring DEBUG2" 
	config-pin -a P9_14 in-
	config-pin -q P9_14

echo "-Configuring CS4"
	config-pin -a P9_15 hi+
	config-pin -q P9_15

echo "-Configuring MOSI/SDI" 
	config-pin -a P9_18 hi+
	config-pin -q P9_18

echo "-Configuring MISO/SDO"
	config-pin -a P9_21 in-
	config-pin -q P9_21

echo "-Configuring SCLK/SCK"
	config-pin -a P9_22 hi+
	config-pin -q P9_22
	
echo "-Configuring FSYNC"
	config-pin -a P9_41 hi-
	config-pin -q P9_41

echo "Booting the PRU with the new firmware..."
	if [ $PRU_CORE -eq 0 ]
	then
		echo "Rebooting PRU0"
		echo "4a334000.pru0" > /sys/bus/platform/drivers/pru-rproc/bind
	else
		echo "Rebooting PRU1"
		echo "4a338000.pru1" > /sys/bus/platform/drivers/pru-rproc/bind
	fi

echo "Finished compilation - SPI communication should be happening..."
