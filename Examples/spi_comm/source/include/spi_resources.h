/* 
 * Copyright (c) 2017 Roger Isaksson (erogisa@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <stdint.h>
#include <stdbool.h>
#include <pru_cfg.h>
#include "spi_defines.h"
#include "mpu_9250_resources.h"

static const uint32_t bank0_set = GPIO_BANK0_SET;
static const uint32_t bank0_clear = GPIO_BANK0_CLEAR;
static const uint32_t bank1_set = GPIO_BANK1_SET;
static const uint32_t bank1_clear = GPIO_BANK1_CLEAR;
static const uint32_t bank2_set = GPIO_BANK2_SET;
static const uint32_t bank2_clear = GPIO_BANK2_CLEAR;
static const uint32_t bank3_set = GPIO_BANK3_SET;
static const uint32_t bank3_clear = GPIO_BANK3_CLEAR;

static const uint32_t bank0_datain = GPIO_BANK0_DATAIN;
static const uint32_t bank1_datain = GPIO_BANK1_DATAIN;
	
static const uint32_t sclk = DIO(SCLK);
static const uint32_t mosi = DIO(MOSI);
static const uint32_t fsync = DIO(FSYNC);
static const uint32_t miso = DIO(MISO);
static const uint32_t debug1 = DIO(DEBUG1);
static const uint32_t debug2 = DIO(DEBUG2);

static const uint32_t cs_array[] = {
						DIO(CS_1),
						DIO(CS_2),
						DIO(CS_3),
						DIO(CS_4)};

static const uint8_t cs_array_nelems = LEN(cs_array);

void setCs(const uint32_t cs);

void clearCs(const uint32_t cs);

void setSclk(void);

void clearSclk(void);

void setMosi(const bool bit);

void clearMosi(void);

void setFsync(const bool bit);

void clearFsync(void);

bool getMiso(void);

void setDebug1(const bool bit);

void clearDebug1(void);

void setDebug2(const bool bit);

void clearDebug2(void);

bool getDebug2(void);

void spiIO(const uint16_t (*mpu_cmd_ptr)[3], volatile uint16_t *misodata, const uint32_t cs_pin);
