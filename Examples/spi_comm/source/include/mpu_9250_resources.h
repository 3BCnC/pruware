/* 
 * Copyright (c) 2017 Roger Isaksson (erogisa@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <stdint.h>
#include "mpu_9250_defines.h"

typedef enum {
	NO_DELAY,
	DELAY200MS,
	I2CDELAY,
} Delays;

typedef enum {
	READ_MAG_X = 0,
	READ_MAG_Y = 1,
	READ_MAG_Z = 2,
	READ_ACC_X = 3,
	READ_ACC_Y = 4,
	READ_ACC_Z = 5,
	READ_GYR_X = 6,
	READ_GYR_Y = 7,
	READ_GYR_Z = 8,
	WRITE, //Write
	READ // Read
} BusOperations;

typedef enum{
	LO_BYTE,
	HI_BYTE
} ByteOrder;

/* Array definition [command, delay_time, R/W]*/

static const uint16_t mpu_setup_commands[][3] = {
									{bytesToShort(MPUREG_PWR_MGMT_1, RESET_DEVICE),DELAY200MS,WRITE},
									{bytesToShort(MPUREG_PWR_MGMT_2, POWER_ON),DELAY200MS,WRITE},
									{bytesToShort(MPUREG_FIFO_EN, DISABLE),NO_DELAY,WRITE},
									{bytesToShort(MPUREG_PWR_MGMT_1, TURN_ON_CLOCK),NO_DELAY,WRITE},
									{bytesToShort(MPUREG_CONFIG, BITS_DLPF_CFG_188HZ),NO_DELAY,WRITE},
									{bytesToShort(MPUREG_SMPLERT_DIV, _1KHZ),NO_DELAY,WRITE},
									{bytesToShort(MPUREG_GYRO_CONFIG, _2000DPS),NO_DELAY,WRITE},
									{bytesToShort(MPUREG_ACCEL_CONFIG_1, _8G),NO_DELAY,WRITE},
									{bytesToShort(MPUREG_ACCEL_CONFIG_2, _1130HZ_DATA_RATE),NO_DELAY,WRITE},
									{bytesToShort(MPUREG_INT_PIN_CFG, INTERRUPT_MODE),NO_DELAY,WRITE},
									{bytesToShort(MPUREG_I2C_USER_CTRL, I2C_MASTER_MODE),NO_DELAY,WRITE},
									{bytesToShort(MPUREG_I2C_MST_CTRL, I2C_MST_CLK_500kHz),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_ADDR, AK8963_ADDRESS),NO_DELAY,WRITE}, /*I2C Address*/
									{bytesToShort(I2C_SLV0_REG, AK8963_CNTL1),NO_DELAY,WRITE}, /*I2C Register*/
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),NO_DELAY,WRITE}, /*I2C Command*/
									{bytesToShort(I2C_SLV0_DO, I2C_POWEROFF_DEVICE),DELAY200MS,WRITE}, /*I2C Data with wait time for transmission and reboot*/
									{bytesToShort(I2C_SLV0_ADDR, AK8963_ADDRESS),NO_DELAY,WRITE}, /*I2C Address*/
									{bytesToShort(I2C_SLV0_REG, AK8963_CNTL1),NO_DELAY,WRITE}, /*I2C Register*/
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),NO_DELAY,WRITE}, /*I2C Command*/
									{bytesToShort(I2C_SLV0_DO, _100HZ_16_BIT),I2CDELAY,WRITE} /*I2C Data with wait time for transmission and reboot*/
									};
									
static const uint16_t setup_cmds_nrows = LEN(mpu_setup_commands);

static const uint16_t mpu_daq_commands[][3] = { 
									/*I2C Magnetometer Data SPI to I2C commands*/
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_ST1),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,READ},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_XOUT_L),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_MAG_X)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_XOUT_H),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_MAG_X)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_YOUT_L),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_MAG_Y)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_YOUT_H),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_MAG_Y)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_ZOUT_L),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_MAG_Z)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_ZOUT_H),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_MAG_Z)},
									
									{bytesToShort(I2C_SLV0_ADDR, (AK8963_ADDRESS | I2C_READ_FLAG)),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_REG, AK8963_ST2),NO_DELAY,WRITE},
									{bytesToShort(I2C_SLV0_CTRL, MPU_I2C_ENABLE_AND_READ),I2CDELAY,WRITE},
									{bytesToShort((EXT_SENS_DATA_00 | MPU_I2C_READ_FLAG),0),NO_DELAY,READ},
									
									/*Accelerometer Data SPI commands*/
									{bytesToShort((MPU_READ_FLAG | ACCEL_XOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_ACC_X)},
									{bytesToShort((MPU_READ_FLAG | ACCEL_XOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_ACC_X)},
									{bytesToShort((MPU_READ_FLAG | ACCEL_YOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_ACC_Y)},
									{bytesToShort((MPU_READ_FLAG | ACCEL_YOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_ACC_Y)},
									{bytesToShort((MPU_READ_FLAG | ACCEL_ZOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_ACC_Z)},
									{bytesToShort((MPU_READ_FLAG | ACCEL_ZOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_ACC_Z)},
									
									/*Gyro Data SPI commands*/
									{bytesToShort((MPU_READ_FLAG | GYRO_XOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_GYR_X)},
									{bytesToShort((MPU_READ_FLAG | GYRO_XOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_GYR_X)},
									{bytesToShort((MPU_READ_FLAG | GYRO_YOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_GYR_Y)},
									{bytesToShort((MPU_READ_FLAG | GYRO_YOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_GYR_Y)},
									{bytesToShort((MPU_READ_FLAG | GYRO_ZOUT_H), 0),NO_DELAY,bytesToShort(HI_BYTE,READ_GYR_Z)},
									{bytesToShort((MPU_READ_FLAG | GYRO_ZOUT_L), 0),NO_DELAY,bytesToShort(LO_BYTE,READ_GYR_Z)},
									};

static const uint16_t daq_cmds_nrows = LEN(mpu_daq_commands);
