/* 
 * Copyright (c) 2017 Roger Isaksson (erogisa@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
// Convenience macro for joining the register and settings
 #define bytesToShort(hibyte, lobyte)		((uint16_t)((((hibyte) & 0xFF) << 8) | ((lobyte) & 0xFF)))
 
 // Convenience macro for determining a 1D/2D array size
 #define LEN(arr) ((int) (sizeof (arr) / sizeof (arr)[0]))
 
/* --------------- MPU REGISTERS ------------- */
#define MPUREG_WHOAMI				(0x75)
#define MPUREG_PWR_MGMT_1			(0x6B)
#define MPUREG_PWR_MGMT_2			(0x6C)
#define MPUREG_FIFO_EN				(0x23)
#define MPUREG_CONFIG				(0x1A)
#define MPUREG_SMPLERT_DIV 			(0x19)
#define MPUREG_GYRO_CONFIG			(0x1B)
#define MPUREG_ACCEL_CONFIG_1		(0x1C)
#define MPUREG_ACCEL_CONFIG_2		(0x1D)
#define MPUREG_INT_PIN_CFG			(0x37)
#define MPUREG_I2C_USER_CTRL		(0x6A)
#define MPUREG_I2C_MST_CTRL		(0x24)

/* -------- MPU REGISTER SETTINGS -------- */

// The Invensense MPU 9250 Power Mgmt Register Settings
#define RESET_DEVICE				(0x80)
#define POWER_ON					(0x0)
#define TURN_ON_CLOCK				(0x0)

// The Invensense MPU 9250 FIFO Register Settings
#define DISABLE					(0x0)

// The Invensense MPU 9250 Interrupt Settings
#define INTERRUPT_MODE				(0x30)

/* --------------- I2C REGISTERS --------------- */

// The Invensense MPU 9250 I2C Specific Registers and Settings. 
#define I2C_SLV0_ADDR				(0x25)
#define I2C_SLV0_REG				(0x26)
#define I2C_SLV0_CTRL				(0x27)
#define I2C_SLV0_DO				(0x63)
#define MPU_I2C_READ_FLAG			(0x80)
#define EXT_SENS_DATA_00			(0x49)
#define I2C_POWEROFF_DEVICE		(0x0)
#define I2C_READ_FLAG				(0x80)
#define MPU_I2C_ENABLE_AND_READ	(0x81)

// The AK8963 I2C Registers
#define AK8963_ADDRESS				(0xC)
#define AK8963_CNTL1				(0xA)
#define AK8963_ST1					(0x2)
#define AK8963_XOUT_L				(0x3)
#define AK8963_XOUT_H				(0x4)
#define AK8963_YOUT_L				(0x5)
#define AK8963_YOUT_H				(0x6)
#define AK8963_ZOUT_L				(0x7)
#define AK8963_ZOUT_H				(0x8)
#define AK8963_ST2					(0x9)

/* --------- I2C REGISTER SETTINGS ---------- */

// The Invensense MPU 9250 I2C Master Settings
#define I2C_MASTER_MODE			(0x20)
#define I2C_MST_CLK_500kHz			(0x9)

/* ------------- SENSOR REGISTERS  ----------- */
// The Invensense MPU Gyro and Accel Regs
#define ACCEL_XOUT_H				(0x3B)
#define ACCEL_XOUT_L				(0x3C)
#define ACCEL_YOUT_H				(0x3D)
#define ACCEL_YOUT_L				(0x3E)
#define ACCEL_ZOUT_H				(0x3F)
#define ACCEL_ZOUT_L				(0x40)

#define GYRO_XOUT_H				(0x43)
#define GYRO_XOUT_L				(0x44)
#define GYRO_YOUT_H				(0x45)
#define GYRO_YOUT_L				(0x46)
#define GYRO_ZOUT_H				(0x47)
#define GYRO_ZOUT_L				(0x48)

/* -------SENSOR REGISTER SETTINGS ------ */

#define MPU_READ_FLAG				(0x80)

// The Invensense MPU 9250 Digital Low Pass Filter Register Settings
#define BITS_DLPF_CFG_188HZ			(0x0)
#define _1KHZ						(0x0)

// The Invensense MPU 9250 Gyro Settings
#define _2000DPS					(0x18)

// The Invensense MPU 9250 Accel Settings
#define _8G						(0x10)
#define _1130HZ_DATA_RATE			(0x0)

// The Invensense MPU Magneto Settings
#define _100HZ_16_BIT				(0x16)

