/* 
 * Copyright (c) 2017 Roger Isaksson (erogisa@gmail.com).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "spi_resources.h"

void setCs(const uint32_t cs){
	registerAccess32(bank1_set) = cs;
}

void clearCs(const uint32_t cs){
	registerAccess32(bank1_clear) = cs;
}

void setSclk(void){
	registerAccess32(bank0_set) = sclk;
}

void clearSclk(void){
	registerAccess32(bank0_clear) = sclk;
}

void setMosi(const bool bit){
	if(bit){
		registerAccess32(bank0_set) = mosi;
	}
}

void clearMosi(void){
	registerAccess32(bank0_clear) = mosi;
}

void setFsync(const bool bit){
	if(bit){
		registerAccess32(bank3_set) = fsync;
	}
}

void clearFsync(void){
	registerAccess32(bank3_clear) = fsync;
}

bool getMiso(void){
	return ((registerAccess32(bank0_datain)) >> MISO) & 1;
}

void setDebug1(const bool bit){
	if(bit){
		registerAccess32(bank0_set) = debug1;
	}
}

void clearDebug1(void){
	registerAccess32(bank0_clear) = debug1;
}

void setDebug2(const bool bit){
	if(bit){
		registerAccess32(bank1_set) = debug2;
	}
}

void clearDebug2(void){
	registerAccess32(bank1_clear) = debug2;
}

bool getDebug2(void){
	return ((registerAccess32(bank1_datain)) >> DEBUG2) & 1;
}

void spiIO(const uint16_t (*mpu_cmd_ptr)[3], volatile uint16_t *misodata, const uint32_t cs_pin){
	
	/* Some excercises in pointer magic */
	
	/*
	const uint16_t (*mpu_cmd_ptr)[3] = &mpu_setup_commands[0];  // Start with the setup commands
	const uint16_t *nrows = &setup_cmds_nrows; // Nr of rows in the setup array 
		
	const uint16_t ((*cmdptr[])[3]) = {mpu_setup_commands, 
								mpu_daq_commands};
								
	const uint16_t *nrowsptr[] = {&setup_cmds_nrows, 
							&daq_cmds_nrows};
	
	*cmdptr[1][0];
	*nrowsptr[0];
	*/	
	
	/*Pull the CS pin low, MPU (with cs_pin) should be ready for IO*/
	clearCs(cs_pin);

	/* Read from the setup commands data array into a register variable */
	const  uint16_t mosi_val = (*mpu_cmd_ptr)[0];
	const  Delays cmd_delay = (Delays)(*mpu_cmd_ptr)[1];
	const  BusOperations bus_cmd = (BusOperations)(((*mpu_cmd_ptr)[2]) & 0x00FF);

	/* Clear any old miso data in the shared memory */
	*misodata = 0;

	register uint16_t miso_val = 0;
	register uint16_t i = 0;

	for (i = 0; i < 16; i++) {
		miso_val = miso_val << 1; // Shift miso value, (initially, i=0, only shifts zeros)

		if ((mosi_val << i) & 0x8000) /* Shift and Check if MSB is high at that bit */
			setMosi(true); /* Set the mosi pin to 1 */
		else
			clearMosi(); /* Set the mosi pin to 0 */

		clearSclk();
		
		if (getMiso()) /* Check if miso bit is high*/
			miso_val |= 0x0001; /* Set the bit to 1 */
		else
			miso_val &= ~(0x0001); /* Set the bit to 0 */

		setSclk();
	}
	
	setCs(cs_pin);
	
	switch(bus_cmd){	/*Reading or writing*/
		case WRITE: /*Do nothing, write (only) to the SPI chip */
			break;
		default:
			(*misodata) = miso_val;
			break;
	}

	switch(cmd_delay){	/*Wait for the MPU to execute/complete/dwell the command*/
		case NO_DELAY:
			__delay_cycles(70); /* Wait for MPU between commands*/
			break;
		case DELAY200MS:
			__delay_cycles(40000000); /* Dwell 200ms after cycling power and resetting SPI IC's*/
			break;
		case I2CDELAY:
			__delay_cycles(5000); /* Dwell for the 8 bit transfer at 200MHz PRU speed (for 500kHz I2C RX/TX)*/
			break;
		default:
			__delay_cycles(70); /* Wait for MPU*/
			break;
	}
}
